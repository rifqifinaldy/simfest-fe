# Ambil base image Node.js dengan versi yang diinginkan
FROM node:18-alpine

# Buat direktori aplikasi di dalam container
WORKDIR /app

# Salin package.json dan package-lock.json (jika ada)
COPY package*.json ./

RUN npm install --legacy-peer-deps
COPY . .
RUN npm run build
EXPOSE 3000
CMD [ "npm", "start" ]