import HomepageLayout from "@/simtest-layouts/homepage";
import Homepage from "@/simtest-page-modules/homepage/home";
import { NextPageWithLayout } from "type/layout.interface";

const Home: NextPageWithLayout = () => {
  return <Homepage />;
};

export default Home;

Home.getLayout = function getLayout(page: React.ReactElement) {
  return <HomepageLayout>{page}</HomepageLayout>;
};
