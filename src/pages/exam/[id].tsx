import React from "react";
import ExamLayout from "@/simtest-layouts/exam";
import TakeExamPages from "@/simtest-page-modules/exam/screens/take-exam";
import { NextPageWithLayout } from "@/simtest-type/layout.interface";
import axios from "axios";
import { GetServerSideProps } from "next";
import ExamProvider from "@/simtest-context/exam.context";
import { IExamResponse } from "@/simtest-type/exam/exam-question.interface";
import { API } from "@/simtest-config/api-collection";

interface ExamProps {
  data: IExamResponse;
  error: boolean;
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context;
  const token = context.req.cookies["st"];
  try {
    const response = await axios.get(
      process.env["NEXT_PUBLIC_BASE_URL"] + API.V1.EXAM.TAKE,
      {
        headers: {
          Authorization: token,
        },
        params: {
          id: query.id,
        },
      }
    );
    const data = await response.data;
    return { props: { data, error: false } };
  } catch (error) {
    return {
      props: {
        error: true,
      },
    };
  }
};

const Exam: NextPageWithLayout<ExamProps> = (props) => {
  return (
    <ExamProvider data={props.data} error={props.error}>
      <TakeExamPages />
    </ExamProvider>
  );
};

export default Exam;

Exam.getLayout = function getLayout(page: React.ReactElement) {
  return <ExamLayout>{page}</ExamLayout>;
};
