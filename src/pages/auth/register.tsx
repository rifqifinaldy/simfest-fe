import AuthLayout from "@/simtest-layouts/auth";
import SignupPages from "@/simtest-page-modules/auth/signup";
import React from "react";
import { NextPageWithLayout } from "type/layout.interface";

const Register: NextPageWithLayout = () => {
  return <SignupPages />;
};

export default Register;

Register.getLayout = function getLayout(page: React.ReactElement) {
  return <AuthLayout>{page}</AuthLayout>;
};
