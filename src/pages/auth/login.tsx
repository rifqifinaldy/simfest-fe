import AuthLayout from "@/simtest-layouts/auth";
import LoginPage from "@/simtest-page-modules/auth/login";
import React from "react";
import { NextPageWithLayout } from "type/layout.interface";

const Login: NextPageWithLayout = () => {
  return <LoginPage />;
};

export default Login;

Login.getLayout = function getLayout(page: React.ReactElement) {
  return <AuthLayout>{page}</AuthLayout>;
};
