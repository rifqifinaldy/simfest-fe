import PageLoader from "@/simtest-components/loader/page-loader";
import useAuthentication from "@/simtest-hook/useAuthentication";
import { NextPageWithLayout } from "@/simtest-type/layout.interface";
import { ChakraBaseProvider, theme } from "@chakra-ui/react";
import { useRouter } from "next/router";
import React, { useEffect } from "react";

const Oauth: NextPageWithLayout = () => {
  const router = useRouter();
  const { loginGoogle } = useAuthentication();

  useEffect(() => {
    if (router.query.code) {
      loginGoogle(router.query.code as string);
    }
  }, [loginGoogle, router.query.code]);

  return (
    <ChakraBaseProvider resetCSS theme={theme}>
      <PageLoader text="Verifying your data" />)
    </ChakraBaseProvider>
  );
};

export default Oauth;

Oauth.getLayout = function getLayout(page: React.ReactElement) {
  return <>{page}</>;
};
