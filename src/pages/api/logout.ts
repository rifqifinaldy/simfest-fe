// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  res.setHeader("Set-Cookie", [
    `usr_dt=deleted; Max-Age=0; path=/`,
    `st=deleted; Max-Age=0; path=/`,
  ]);
  return res.status(200).json({ message: "You are logged out" });
}
