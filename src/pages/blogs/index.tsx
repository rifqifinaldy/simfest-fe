import HomepageLayout from "@/simtest-layouts/homepage";
import Blogpage from "@/simtest-page-modules/homepage/blogs";
import { NextPageWithLayout } from "type/layout.interface";
import { createClient } from "contentful";
import { ContentfulReturnType } from "@/simtest-type/contentful/contentful.interface";
import { IBlog } from "@/simtest-type/blogs/blog.interface";

export async function getStaticProps() {
  const client = createClient({
    space: "bjluxsx1n4vp",
    accessToken: "YH47TIY24pT5-cJGOkZjwBMDYfyYfjanHMRQcFFZ0d8",
  });

  const res = await client.getEntries({
    content_type: "stcom",
  });

  return {
    props: {
      data: res,
    },
    revalidate: 1,
  };
}

const Blog: NextPageWithLayout<{
  data: ContentfulReturnType;
}> = ({ data }) => {
  return <Blogpage blogs={data.items as IBlog[]} />;
};

export default Blog;

Blog.getLayout = function getLayout(page: React.ReactElement) {
  return <HomepageLayout>{page}</HomepageLayout>;
};
