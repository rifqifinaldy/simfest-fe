import HomepageLayout from "@/simtest-layouts/homepage";
import { NextPageWithLayout } from "type/layout.interface";
import { ContentfulReturnType } from "@/simtest-type/contentful/contentful.interface";
import { createClient } from "contentful";
import { GetStaticProps } from "next";
import BlogsDetail from "@/simtest-page-modules/homepage/blogs-detail";
import { IBlog } from "@/simtest-type/blogs/blog.interface";
import PageLoader from "@/simtest-components/loader/page-loader";

const client = createClient({
  space: "bjluxsx1n4vp",
  accessToken: "YH47TIY24pT5-cJGOkZjwBMDYfyYfjanHMRQcFFZ0d8",
});

export const getStaticPaths = async () => {
  const res = await client.getEntries({
    content_type: "stcom",
  });

  const paths = res.items.map((item) => {
    return {
      params: {
        slug: item.fields.slug,
      },
    };
  });

  return {
    paths,
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { items } = await client.getEntries({
    content_type: "stcom",
    "fields.slug": params!.slug,
  });

  return {
    props: {
      blog: items[0],
    },
    revalidate: 1,
  };
};

const Slug: NextPageWithLayout<{
  blog: ContentfulReturnType;
}> = ({ blog }) => {
  if (!blog) {
    return <PageLoader text="Loading Pages..." />;
  }

  return <BlogsDetail blog={blog as unknown as IBlog} />;
};

export default Slug;

Slug.getLayout = function getLayout(page: React.ReactElement) {
  return <HomepageLayout>{page}</HomepageLayout>;
};
