import { NextPageWithLayout } from "@/simtest-type/layout.interface";
import { Flex, Text } from "@chakra-ui/react";
import { useRouter } from "next/router";
import React from "react";
import { AiFillWarning } from "react-icons/ai";

const NotFound: NextPageWithLayout = () => {
  const router = useRouter();
  return (
    <Flex
      flexDir="column"
      alignItems="center"
      justifyContent="center"
      h="full"
      w="full"
    >
      <AiFillWarning size="92px" color="#FFC007" />
      <Text fontSize="48px">Upsss.. Page Not Found</Text>
      <Text fontWeight="400">
        Click{" "}
        <Text
          cursor="pointer"
          as="span"
          onClick={() => router.back()}
          color="cyan"
        >
          here{" "}
        </Text>
        to go back to previous page
      </Text>
    </Flex>
  );
};

export default NotFound;

NotFound.getLayout = function getLayout(page: React.ReactElement) {
  return (
    <Flex
      flexDir="column"
      h="100vh"
      justifyContent="center"
      alignItems="center"
    >
      {page}
    </Flex>
  );
};
