/* eslint-disable @next/next/no-img-element */
import { AppPropsWithLayout } from "type/layout.interface";
import DashboardLayout from "@/simtest-layouts/dashboard";
import Head from "next/head";
import GlobalProvider from "@/simtest-context/global.context";
import "../styles/index.css";
import { Provider } from "react-redux";
import { store } from "@/simtest-redux/store";
import Script from "next/script";
import { useEffect } from "react";
import { useRouter } from "next/router";

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? ((page) => page);
  const router = useRouter();

  useEffect(() => {
    const handleRouteChange = (url: string) => {
      if (window.gtag) {
        window.gtag("config", "G-ZN4B27JFX0", {
          page_path: url,
        });
      }
    };

    router.events.on("routeChangeComplete", handleRouteChange);

    // Clean up the event listener when the component unmounts
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router]);

  return getLayout(
    <>
      <Head>
        <title>Simulasi Test</title>
        <meta
          name="description"
          content="Siapkan dirimu untuk sukses! Temukan latihan soal terbaik untuk persiapan ujian BUMN, CPNS, dan lebih banyak lagi di https://simulasitest.com/. Dapatkan akses ke koleksi soal terlengkap dan simulasi ujian yang membantu. Mulai persiapanmu sekarang untuk mengamankan masa depan karirmu!"
        />
        <meta name="title" content="Simulasi Test" />
        <meta
          name="keywords"
          content="BUMN, CPNS, LPDP, TOEFL, Latihan Soal, Simulasi, Ujian, Persiapan Tes, Pelatihan Tes, Karir, Pendidikan"
        />
        <link rel="icon" href="/favicon.ico" />
        <noscript>
          <img
            height="1"
            width="1"
            style={{ display: "none" }}
            src="https://www.facebook.com/tr?id=877447497473818&ev=PageView&noscript=1"
            alt="simulasi-test.com"
          />
        </noscript>
      </Head>
      <Script
        id="simulasi-test-fb-pixel"
        dangerouslySetInnerHTML={{
          __html: `
              !function(f,b,e,v,n,t,s)
              {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
              n.callMethod.apply(n,arguments):n.queue.push(arguments)};
              if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
              n.queue=[];t=b.createElement(e);t.async=!0;
              t.src=v;s=b.getElementsByTagName(e)[0];
              s.parentNode.insertBefore(t,s)}(window, document,'script',
              'https://connect.facebook.net/en_US/fbevents.js');
              fbq('init', '877447497473818');
              fbq('track', 'PageView');
              `,
        }}
      />
      <Script
        async
        src="https://www.googletagmanager.com/gtag/js?id=G-ZN4B27JFX0"
      />
      <Script
        id="google-analytics-script-login-stcom"
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-ZN4B27JFX0');
          `,
        }}
      />
      <Provider store={store}>
        <GlobalProvider>
          {Component.getLayout ? (
            <Component {...pageProps} />
          ) : (
            <DashboardLayout>
              <Component {...pageProps} />
            </DashboardLayout>
          )}
        </GlobalProvider>
      </Provider>
    </>
  );
}

export default MyApp;
