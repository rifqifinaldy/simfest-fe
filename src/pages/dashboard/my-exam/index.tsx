import DashboardLayout from "@/simtest-layouts/dashboard";
import DashboardMyExamPage from "@/simtest-page-modules/dashboard/screens/my-exam";
import React from "react";
import { NextPageWithLayout } from "type/layout.interface";

const DashboardMyExam: NextPageWithLayout = () => {
  return <DashboardMyExamPage />;
};

export default DashboardMyExam;

DashboardMyExam.getLayout = function getLayout(page: React.ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
