import DashboardLayout from "@/simtest-layouts/dashboard";
import DashboardCartPage from "@/simtest-page-modules/dashboard/screens/carts";
import React from "react";
import { NextPageWithLayout } from "type/layout.interface";

const DashboardCart: NextPageWithLayout = () => {
  return <DashboardCartPage />;
};

export default DashboardCart;

DashboardCart.getLayout = function getLayout(page: React.ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
