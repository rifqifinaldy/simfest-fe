import DashboardLayout from "@/simtest-layouts/dashboard";
import DashboardPackagePage from "@/simtest-page-modules/dashboard/screens/package";
import React from "react";
import { NextPageWithLayout } from "type/layout.interface";

const DashboardProduct: NextPageWithLayout = () => {
  return <DashboardPackagePage />;
};

export default DashboardProduct;

DashboardProduct.getLayout = function getLayout(page: React.ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
