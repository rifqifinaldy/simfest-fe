import DashboardLayout from "@/simtest-layouts/dashboard";
import DashboardHomepage from "@/simtest-page-modules/dashboard/screens/my-performance";
import { NAVIGATION } from "@/simtest-type/navigation.interface";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { NextPageWithLayout } from "type/layout.interface";

const Dashboard: NextPageWithLayout = () => {
  const router = useRouter();

  useEffect(() => {
    router.push(NAVIGATION.DASHBOARD_MY_EXAM);
  }, [router]);

  return <DashboardHomepage />;
};

export default Dashboard;

Dashboard.getLayout = function getLayout(page: React.ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
