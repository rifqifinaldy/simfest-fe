import DashboardLayout from "@/simtest-layouts/dashboard";
import DashboardTransactionPage from "@/simtest-page-modules/dashboard/screens/transactions";
import React from "react";
import { NextPageWithLayout } from "type/layout.interface";

const DashboardTransaction: NextPageWithLayout = () => {
  return <DashboardTransactionPage />;
};

export default DashboardTransaction;

DashboardTransaction.getLayout = function getLayout(page: React.ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
