import React from "react";
import ExamLayout from "@/simtest-layouts/exam";
import { NextPageWithLayout } from "@/simtest-type/layout.interface";
import axios from "axios";
import { GetServerSideProps } from "next";
import { IDiscussionResponse } from "@/simtest-type/discussion/discussion.interface";
import DiscussionProvider from "@/simtest-context/discussion.context";
import DiscussionPages from "@/simtest-page-modules/exam/screens/discussion";
import { API } from "@/simtest-config/api-collection";

interface ExamProps {
  data: IDiscussionResponse;
  error: boolean;
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context;
  const token = context.req.cookies["st"];
  try {
    const response = await axios.get(
      process.env["NEXT_PUBLIC_BASE_URL"] +
        API.V1.EXAM.DISCUSSION(query.id as string),
      {
        headers: {
          Authorization: token,
        },
        params: {
          id: query.id,
        },
      }
    );
    const data = await response.data;
    return { props: { data, error: false } };
  } catch (error) {
    return {
      props: {
        error: true,
      },
    };
  }
};

const Exam: NextPageWithLayout<ExamProps> = (props) => {
  return (
    <DiscussionProvider data={props.data} error={props.error}>
      <DiscussionPages />
    </DiscussionProvider>
  );
};

export default Exam;

Exam.getLayout = function getLayout(page: React.ReactElement) {
  return <ExamLayout>{page}</ExamLayout>;
};
