import { Flex, Text } from "@chakra-ui/react";
import React from "react";

interface DashboardHeaderProps {
  title: string;
  subtitle?: string;
}

const DashboardHeader: React.FC<DashboardHeaderProps> = ({
  title,
  subtitle,
}) => {
  return (
    <Flex flexDir="column">
      <Text as="h1" variant="heading-h4" mb="10px">
        {title}
      </Text>
      {subtitle && <Text variant="body-large-regular">{subtitle}</Text>}
    </Flex>
  );
};

export default DashboardHeader;
