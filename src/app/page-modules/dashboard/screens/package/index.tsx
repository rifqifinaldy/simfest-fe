import { Box, Button, Flex, Grid, GridItem } from "@chakra-ui/react";
import React, { useCallback, useContext, useEffect, useState } from "react";
import DashboardHeader from "../../components/dashboard-header";
import { TProductType } from "@/simtest-type/products/product.interface";
import SampleTestCard from "@/simtest-components/cards/sample-test-card";
import SkeletonCard from "@/simtest-components/cards/skeleton-card";
import { useRouter } from "next/router";
import { NAVIGATION } from "@/simtest-type/navigation.interface";
import {
  GlobalContext,
  IGlobalContext,
} from "@/simtest-context/global.context";
import PackageModal from "@/simtest-page-modules/homepage/home/section/package-question/components/PackageModal";
import { IQuestionPackageRes } from "@/simtest-type/package/package.interface";
import useCart from "@/simtest-hook/useCart";
import usePackage from "@/simtest-hook/usePackage";
import usePagination from "@/simtest-hook/usePagination";

const DashboardPackagePage: React.FC = () => {
  const { modal } = useContext(GlobalContext) as IGlobalContext;
  const router = useRouter();
  const { addToCart: addCartList, refetch } = useCart();
  const { getPackageList, packageList, pending, packageListMeta } =
    usePackage();
  const [filter, setFilter] = useState<number>();
  const { pageSize, currentPage, renderPageNumber } = usePagination({
    currentCount: packageList.length || 0,
    totalCount: packageListMeta?.count || 0,
    siblingCount: 1,
  });

  useEffect(() => {
    getPackageList({
      page: currentPage,
      length: pageSize,
      package_id: filter,
    });
  }, [currentPage, filter, getPackageList, pageSize, refetch]);

  const addToCart = useCallback(
    (id: string) => {
      addCartList(id);
    },
    [addCartList]
  );

  const handleAction = useCallback(
    (isBought: boolean, data: IQuestionPackageRes) => {
      if (isBought) {
        router.push({
          pathname: NAVIGATION.DASHBOARD_MY_EXAM,
        });
      } else {
        modal?.onOpen();
        modal?.setContent({
          size: "md",
          closeButton: true,
          body: <PackageModal addToCart={(id) => addToCart(id)} data={data} />,
        });
      }
    },
    [addToCart, modal, router]
  );

  return (
    <>
      <Box>
        <DashboardHeader
          title="Pilihan Test Untukmu"
          subtitle="Beli paket sesuai kebutuhan kamu"
        />
        <Flex gap="10px" mt="20px">
          <Button
            borderRadius="20px"
            colorScheme="blue"
            size="sm"
            variant={filter === undefined ? "solid" : "outline"}
            fontSize="16px"
            onClick={() => setFilter(undefined)}
          >
            All Program
          </Button>
          <Button
            borderRadius="20px"
            colorScheme="blue"
            variant={filter === 2 ? "solid" : "outline"}
            size="sm"
            fontSize="16px"
            onClick={() => setFilter(2)}
          >
            BUMN
          </Button>
          <Button
            borderRadius="20px"
            colorScheme="blue"
            variant={filter === 1 ? "solid" : "outline"}
            size="sm"
            fontSize="16px"
            onClick={() => setFilter(1)}
          >
            LPDP
          </Button>
          <Button
            borderRadius="20px"
            colorScheme="blue"
            variant={filter === 3 ? "solid" : "outline"}
            size="sm"
            fontSize="16px"
            onClick={() => setFilter(3)}
          >
            CPNS
          </Button>
        </Flex>
        <Grid
          my="20px"
          templateColumns={{
            base: "repeat(1, 1fr)",
            sm: "repeat(2, 1fr)",
            lg: "repeat(3, 1fr)",
            xl: "repeat(4, 1fr)",
          }}
          gap="20px"
        >
          {pending && <SkeletonCard count={4} />}
          {packageList?.map((data) => {
            return (
              <GridItem key={data.id}>
                <SampleTestCard
                  moduleType={data?.package?.name as TProductType}
                  name={data.name}
                  duration={data.time}
                  quantity={data.total_question}
                  status={data.user_package_id > 0}
                  action={() => handleAction(data.user_package_id > 0, data)}
                />
              </GridItem>
            );
          })}
        </Grid>
        {renderPageNumber}
      </Box>
    </>
  );
};

export default DashboardPackagePage;
