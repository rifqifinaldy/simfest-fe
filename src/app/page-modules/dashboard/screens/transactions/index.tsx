import React, { useEffect } from "react";
import { Box, Grid, GridItem } from "@chakra-ui/react";
import DashboardHeader from "../../components/dashboard-header";
import useOrder from "@/simtest-hook/useOrder";
import EmptyState from "@/simtest-components/empty-state";
import MyOrderCard from "@/simtest-components/cards/my-order-card";
import Head from "next/head";

const DashboardTransactionPage: React.FC = () => {
  const { getOrderList, orderList, pending } = useOrder();

  useEffect(() => {
    getOrderList();
  }, [getOrderList]);

  if (!orderList?.length && !pending) {
    return <EmptyState title="Anda belum melakukan transaksi apapun" />;
  }

  return (
    <>
      <Head>
        <script
          src={`https://app${
            process.env.NEXT_PUBLIC_PRODUCTION !== "true" ? ".sandbox" : ""
          }.midtrans.com/snap/snap.js`}
          data-client-key={process.env.NEXT_PUBLIC_MIDTRANS_CLIENT_KEY}
          defer
        />
      </Head>
      <Box>
        <DashboardHeader
          title="Daftar Transaksi"
          subtitle="Semua transaksi ujian yang pernah anda lakukan"
        />
        <Grid
          templateColumns={{
            base: "repeat(1, 1fr)",
            md: "repeat(2, 1fr)",
            lg: "repeat(3, 1fr)",
          }}
          gap="20px"
          py="20px"
        >
          {orderList?.map((data) => {
            return (
              <GridItem key={data.id}>
                <MyOrderCard
                  id={data.id}
                  status={data.status}
                  paidAt={data.expired_at as string}
                  productList={data.details}
                  totalAmount={data.total_amount}
                  paymentToken={data.payment_token}
                />
              </GridItem>
            );
          })}
        </Grid>
      </Box>
    </>
  );
};

export default DashboardTransactionPage;
