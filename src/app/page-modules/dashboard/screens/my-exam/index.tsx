import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Button,
  Flex,
  Grid,
  GridItem,
  Image,
  Text,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import DashboardHeader from "../../components/dashboard-header";
import { EProductType } from "@/simtest-type/products/product.interface";
import { renderModuleType } from "helper/package-question/render-module-type";
import MyExamCard from "@/simtest-components/cards/my-exam-card";
import EmptyState from "@/simtest-components/empty-state";
import { useRouter } from "next/router";
import { NAVIGATION } from "@/simtest-type/navigation.interface";
import usePackage from "@/simtest-hook/usePackage";

const DashboardMyExamPage: React.FC = () => {
  const router = useRouter();
  const { myPackage: list, getMyPackage, pending } = usePackage();
  const [filter, setFilter] = useState<EProductType | "all">("all");

  const filteredPackageQuestion = () => {
    if (filter === "all") {
      return list;
    } else {
      return list.filter((data) => data.package.name === filter);
    }
  };

  useEffect(() => {
    getMyPackage({
      page: 1,
      length: 10,
    });
  }, [getMyPackage]);

  if (!list?.length && !pending) {
    return (
      <EmptyState
        title="Anda Belum Memiliki Ujian Untuk Diselesaikan"
        showButton={true}
        buttonText="Cari Ujian"
        buttonAction={() => router.push(NAVIGATION.DASHBOARD_PRODUCT)}
      />
    );
  }

  return (
    <Box>
      <DashboardHeader
        title="Ujian saya"
        subtitle="Katalog ujian yang anda miliki"
      />
      <Flex gap="10px" mt="20px">
        <Button
          borderRadius="20px"
          colorScheme="blue"
          variant={filter === "all" ? "solid" : "outline"}
          size="sm"
          fontSize="16px"
          onClick={() => setFilter("all")}
        >
          All Program
        </Button>
        <Button
          borderRadius="20px"
          colorScheme="blue"
          variant={filter === EProductType.BUMN ? "solid" : "outline"}
          size="sm"
          fontSize="16px"
          onClick={() => setFilter(EProductType.BUMN)}
        >
          BUMN
        </Button>
        <Button
          borderRadius="20px"
          colorScheme="blue"
          variant={filter === EProductType.LPDP ? "solid" : "outline"}
          size="sm"
          fontSize="16px"
          onClick={() => setFilter(EProductType.LPDP)}
        >
          LPDP
        </Button>
        <Button
          borderRadius="20px"
          colorScheme="blue"
          variant={filter === EProductType.CPNS ? "solid" : "outline"}
          size="sm"
          fontSize="16px"
          onClick={() => setFilter(EProductType.CPNS)}
        >
          CPNS
        </Button>
      </Flex>
      {list?.length > 0 &&
        filteredPackageQuestion().map((data) => {
          return (
            <Box key={data.package.id} mt="20px">
              <Flex
                p="10px"
                borderRadius="8px"
                gap="10px"
                alignItems="center"
                background={renderModuleType(data.package.name).color}
              >
                <Image
                  src={renderModuleType(data.package.name).logo}
                  alt={data.package.name}
                />
                <Text variant="body-medium-bold" color="white">
                  {data.package.name}
                </Text>
              </Flex>
              <Accordion
                bg="neutral.0"
                key={data.package.id}
                my="10px"
                allowToggle
              >
                {data.my_package.map((my_package) => {
                  return (
                    <AccordionItem
                      borderRadius="8px"
                      my="10px"
                      key={my_package.id}
                    >
                      <AccordionButton
                        py="20px"
                        display="flex"
                        justifyContent="space-between"
                        shadow="sm"
                      >
                        <Text variant="heading-h6">
                          {my_package.package_question.name}
                        </Text>
                        <AccordionIcon />
                      </AccordionButton>
                      <AccordionPanel py="0" px="10px">
                        <Grid
                          templateColumns={{
                            base: "repeat(1, 1fr)",
                            md: "repeat(3, 1fr)",
                          }}
                          gap="10px"
                          pb="20px"
                        >
                          {my_package.sections.length > 0 &&
                            my_package.sections.map((exam) => {
                              return (
                                <GridItem key={exam.id}>
                                  <MyExamCard
                                    id={exam.id as string}
                                    package_name={
                                      my_package.package_question.name
                                    }
                                    title={exam.name}
                                    quantity={exam.total_question}
                                    duration={exam.time}
                                    correctAnswer={exam.correct_answer}
                                    isAnswered={Boolean(exam.exam_id)}
                                    exam_id={exam.exam_id as string}
                                  />
                                </GridItem>
                              );
                            })}
                        </Grid>
                      </AccordionPanel>
                    </AccordionItem>
                  );
                })}
              </Accordion>
            </Box>
          );
        })}
    </Box>
  );
};

export default DashboardMyExamPage;
