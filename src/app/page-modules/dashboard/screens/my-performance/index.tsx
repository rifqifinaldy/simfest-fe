import { Box } from "@chakra-ui/react";
import React from "react";
import DashboardHeader from "../../components/dashboard-header";

const DashboardHomepage: React.FC = () => {
  return (
    <Box>
      <DashboardHeader
        title="My Test Performance"
        subtitle="Hasil performa tes yang pernah kamu kerjakan"
      />
    </Box>
  );
};

export default DashboardHomepage;
