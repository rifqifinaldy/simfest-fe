import VoucherInput from "@/simtest-components/forms/voucher-input";
import TNC from "@/simtest-components/popup/simfest-tnc";
import {
  GlobalContext,
  IGlobalContext,
} from "@/simtest-context/global.context";
import useCart from "@/simtest-hook/useCart";
import useOrder from "@/simtest-hook/useOrder";
import { Box, Button, Checkbox, Flex, Text } from "@chakra-ui/react";
import { rupiah } from "helper/idr-converter";
import Head from "next/head";
import React, { useCallback, useContext, useEffect, useState } from "react";
import { FieldValues } from "react-hook-form";
import { AiFillDelete, AiOutlineArrowRight } from "react-icons/ai";

const CartList: React.FC<{ type: "drawer" | "section" }> = ({ type }) => {
  const {
    getCartList,
    cartList,
    deleteCart,
    resetVoucher,
    resetCart,
    totalAmount,
    voucher,
    error,
    refetch,
    success,
  } = useCart();
  const { createOrder } = useOrder();
  const { modal, drawer } = useContext(GlobalContext) as IGlobalContext;
  const [showInputVoucher, setShowInputVoucher] = useState<boolean>(false);
  const [agreed, setAgreed] = useState<boolean>(false);

  useEffect(() => {
    getCartList("");
    resetCart();
  }, [getCartList, resetCart, refetch]);

  const submitVoucher = useCallback(
    (voucher: FieldValues) => {
      getCartList(voucher.voucher);
    },
    [getCartList]
  );

  useEffect(() => {
    if (success && type === "drawer") {
      drawer?.setContent({
        ...drawer.content,
        header: `Item Keranjang anda (${cartList?.length || 0})`,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cartList?.length, success, type]);

  const openTNC = useCallback(() => {
    modal?.onOpen();
    modal?.setContent({
      header: "Term and Condition",
      body: <TNC />,
      closeButton: false,
      size: "2xl",
      footer: (
        <Button
          w="full"
          colorScheme="blue"
          onClick={() => {
            setAgreed(true);
            modal.onClose();
          }}
        >
          I Understand
        </Button>
      ),
    });
  }, [modal]);

  return (
    <>
      <Head>
        <script
          src={`https://app${
            process.env.NEXT_PUBLIC_PRODUCTION !== "true" ? ".sandbox" : ""
          }.midtrans.com/snap/snap.js`}
          data-client-key={process.env.NEXT_PUBLIC_MIDTRANS_CLIENT_KEY}
          defer
        />
      </Head>
      <Flex flexDir="column">
        {cartList?.map((cart) => {
          return (
            <Flex
              key={cart.id}
              alignItems="center"
              justifyContent="space-between"
              borderTop="1px solid #B1B8D0"
              borderBottom="1px solid #B1B8D0"
              p="20px"
            >
              <Box w="33%">
                <Text variant="heading-h5">
                  Tryout Soal {cart?.package_question?.name}
                </Text>
              </Box>
              <Box w="33%">
                <Text variant="heading-h5">
                  {rupiah.format(cart?.package_question?.price)}
                </Text>
              </Box>
              <Box>
                <AiFillDelete
                  onClick={() => deleteCart(cart?.id)}
                  cursor="pointer"
                  size="21px"
                />
              </Box>
            </Flex>
          );
        })}
        <Flex py="20px" flexDir="column" borderBottom="1px solid #B1B8D0">
          {showInputVoucher ? (
            <VoucherInput
              resetVoucher={() => resetVoucher()}
              submitVoucher={(voucher) => submitVoucher(voucher)}
              isError={Boolean(error)}
            />
          ) : (
            <Text display="inline" mb="20px">
              Gunakan Kupon Klik{" "}
              <Text
                role="button"
                display="inline"
                as="span"
                textDecoration="underline"
                color="primary.800"
                fontWeight="bold"
                onClick={() => setShowInputVoucher((prev) => !prev)}
              >
                Disini
              </Text>
            </Text>
          )}
          <Text variant="body-xlarge-medium">Ringkasan Pesanan</Text>
          <Flex
            justifyContent="space-between"
            alignItems="center"
            border="1px solid #B1B8D0"
            p="20px"
          >
            <Text
              fontSize="20px"
              fontWeight="medium"
              color="#1D2433"
              opacity="0.65"
            >
              Total Pembayaran
            </Text>
            <Text variant="heading-h5">
              {rupiah.format(totalAmount - voucher.total_discount)}
            </Text>
          </Flex>
          <Flex alignItems="center" gap="10px">
            <Checkbox
              onChange={() => {
                setAgreed((prev) => !prev);
              }}
            />
            <Text my="20px">
              Saya telah membaca dan setuju dengan{" "}
              <Text
                as="span"
                role="button"
                color="primary.900"
                onClick={() => openTNC()}
              >
                Syarat & Ketentuan
              </Text>{" "}
              yang berlaku
            </Text>
          </Flex>
          <Button
            isDisabled={!cartList?.length || !agreed}
            onClick={() => createOrder(voucher.voucher_code)}
            colorScheme="blue"
            size="lg"
            rightIcon={<AiOutlineArrowRight />}
          >
            Bayar
          </Button>
        </Flex>
        {voucher.voucher_status && (
          <Flex py="20px" flexDir="column">
            <Text variant="heading-h6" textAlign="center" my="10px">
              Selamat! Kamu berhak mendapatkan diskon
            </Text>
            <Flex
              justifyContent="space-between"
              alignItems="center"
              border="1px solid #B1B8D0"
              p="20px"
              my="20px"
            >
              <Text variant="body-large-medium">Potongan harga :</Text>
              <Flex alignItems="center" gap="10px">
                <Text variant="heading-h5">
                  {rupiah.format(totalAmount - voucher.total_discount)}
                </Text>
                <Text
                  color="#1D2433"
                  opacity="0.65"
                  fontSize="14px"
                  textDecoration="line-through"
                >
                  {rupiah.format(totalAmount)}
                </Text>
              </Flex>
            </Flex>
          </Flex>
        )}
      </Flex>
    </>
  );
};

export default CartList;
