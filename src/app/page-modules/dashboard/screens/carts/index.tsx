import { Box } from "@chakra-ui/react";
import React from "react";
import DashboardHeader from "../../components/dashboard-header";
import useCart from "@/simtest-hook/useCart";
import CartList from "./components/cart-list";

const DashboardCartPage: React.FC = () => {
  const { cartList } = useCart();

  return (
    <>
      <Box>
        <DashboardHeader
          title={`Item keranjang Anda (${cartList?.length || 0})`}
        />
        <CartList type="section" />
      </Box>
    </>
  );
};

export default DashboardCartPage;
