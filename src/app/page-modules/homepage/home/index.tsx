import React, { useContext, useEffect } from "react";

import BannerHomepageSection from "@/simtest-page-modules/homepage/home/section/banner-homepage";

import { IQuestionPackageRes } from "type/package/package.interface";
import AboutUsSection from "./section/about-us";
import TryNowSection from "./section/try-now";
import PackageQuestionSection from "./section/package-question";
import FaqSection from "./section/faq";
import {
  GlobalContext,
  IGlobalContext,
} from "@/simtest-context/global.context";
import useCart from "@/simtest-hook/useCart";
import usePackage from "@/simtest-hook/usePackage";

const Homepage: React.FC = () => {
  const { user } = useContext(GlobalContext) as IGlobalContext;
  const {
    homepagePackage,
    packageList,
    pending,
    getHomepagePackage,
    getPackageList,
  } = usePackage();
  const { getCartList, resetCart } = useCart();

  useEffect(() => {
    if (user?.profile?.token) {
      getPackageList({
        page: 1,
        length: 12,
      });
      getCartList("");
    } else {
      getHomepagePackage();
    }
    return () => {
      resetCart();
    };
  }, [
    getCartList,
    getHomepagePackage,
    getPackageList,
    resetCart,
    user?.profile?.token,
  ]);

  return (
    <>
      <BannerHomepageSection />
      <PackageQuestionSection
        loading={pending}
        packageQuestion={
          user?.profile?.token
            ? packageList
            : (homepagePackage as IQuestionPackageRes[])
        }
      />
      <AboutUsSection />
      <FaqSection />
      <TryNowSection />
    </>
  );
};

export default Homepage;
