import React from "react";
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Flex,
  ListItem,
  OrderedList,
  Text,
} from "@chakra-ui/react";
import Link from "next/link";
import { SOCIAL } from "helper/social-media";

const FaqSection: React.FC = () => {
  const faqContent = [
    {
      id: 1,
      question: "Siapa saja yang boleh mengikuti tes ini?",
      anwer: (
        <Text>
          Semua orang dapat mencoba latihan tes di SimulasiTest.com. Baik Anda
          seorang pelajar, profesional, atau hanya ingin menguji pengetahuan
          Anda, platform kami terbuka untuk semua kalangan yang tertarik untuk
          menguji kemampuan mereka.
        </Text>
      ),
    },
    {
      id: 2,
      question: "Bagaimana cara membeli paket soal?",
      anwer: (
        <>
          <Text mb="10px">
            Untuk membeli, cukup ikut langkah-langkah berikut
          </Text>
          <OrderedList>
            <ListItem>Pilih paket soal yang Anda inginkan.</ListItem>
            <ListItem>Tambahkan ke keranjang belanja Anda.</ListItem>
            <ListItem>
              Jika Anda memiliki kode voucher, masukkan kode tersebut.
            </ListItem>
            <ListItem>Pilih &apos;Order Sekarang&apos;.</ListItem>
            <ListItem>
              Lakukan pembayaran melalui salah satu metode pembayaran yang
              tersedia.
            </ListItem>
            <ListItem>Paket soal akan ditambahkan ke dashboard Anda.</ListItem>
          </OrderedList>
        </>
      ),
    },
    {
      id: 3,
      question: "Bagaimana cara mendapatkan voucher promo?",
      anwer: (
        <>
          <Text mb="10px">Mendapatkan voucher promo sangat mudah!</Text>
          <OrderedList>
            <ListItem>
              Kunjungi halaman Instagram kami di @simulasitestdotcom.
            </ListItem>
            <ListItem>
              Periksa bagian highlights untuk postingan terbaru.
            </ListItem>
            <ListItem>
              Bagikan postingan tersebut kepada tiga teman Anda.
            </ListItem>
            <ListItem>Kirim pesan langsung kepada kami di Instagram..</ListItem>
            <ListItem>
              Kami akan mengirimkan kode voucher untuk satu paket tes
            </ListItem>
          </OrderedList>
        </>
      ),
    },
    {
      id: 4,
      question: "Ada pertanyaan atau keluhan?",
      anwer: (
        <>
          <Text>
            Jika Anda memiliki pertanyaan atau membutuhkan bantuan, jangan ragu
            untuk menghubungi tim layanan pelanggan kami. Anda dapat menghubungi
            kami melalui{" "}
            <Link href={SOCIAL.TELEGRAM} target="_blank">
              link ini
            </Link>{" "}
            yang akan menghubungkan Anda langsung ke dukungan pelanggan WhatsApp
            kami. Kami di sini untuk membantu dan memastikan pengalaman Anda
            dengan SimulasiTest.com berjalan lancar dan menyenangkan.
          </Text>
        </>
      ),
    },
  ];

  return (
    <Flex
      id="faq"
      as="section"
      py={{ base: "40px", lg: "80px" }}
      justifyContent="space-between"
      gap={{ base: "10px", lg: "80px" }}
      flexDirection={{ base: "column", lg: "row" }}
    >
      <Box w={{ base: "100%", lg: "30%" }}>
        <Text variant="heading-h3" mb="0.5em">
          Frequently Asked Questions
        </Text>
        <Text variant="body-large-regular">
          Masih bingung atau ragu? Hubungi kami di nomor{" "}
          <Link href={SOCIAL.TELEGRAM} target="_blank">
            <span>+{SOCIAL.WA_PHONE_NUMBER}</span>
          </Link>
        </Text>
      </Box>
      <Box pl={{ base: "0", lg: "40px" }} w={{ base: "100%", lg: "70%" }}>
        <Accordion allowToggle>
          {faqContent.map((data) => {
            return (
              <AccordionItem key={data.id}>
                <AccordionButton
                  py="20px"
                  display="flex"
                  justifyContent="space-between"
                >
                  <Flex
                    alignItems={{ base: "flex-start", lg: "center" }}
                    gap="40px"
                  >
                    <Text variant="heading-h4" color="primary.700">
                      0{data.id}
                    </Text>
                    <Text variant={{ base: "heading-h6", md: "heading-h4" }}>
                      {data.question}
                    </Text>
                  </Flex>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel>
                  <Box ml={{ base: "0", lg: "70px" }}>{data.anwer}</Box>
                </AccordionPanel>
              </AccordionItem>
            );
          })}
        </Accordion>
      </Box>
    </Flex>
  );
};

export default FaqSection;
