import { IQuestionPackageRes } from "@/simtest-type/package/package.interface";
import { Box, Button, Flex, Text } from "@chakra-ui/react";
import { rupiah } from "helper/idr-converter";
import { renderModuleType } from "helper/package-question/render-module-type";
import React from "react";
import {
  AiFillCheckCircle,
  AiOutlineClockCircle,
  AiOutlineFileText,
} from "react-icons/ai";

interface PackageModalProps {
  data: IQuestionPackageRes;
  addToCart: (id: string) => void;
}

const PackageModal: React.FC<PackageModalProps> = ({ data, addToCart }) => {
  return (
    <Flex flexDir="column">
      <Box
        w="full"
        h="28px"
        position="absolute"
        top="-5"
        left="0"
        borderRadius="3px 3px 0px 0px"
        background={renderModuleType(data.package.name).color}
      />
      <Text variant="heading-h5" textAlign="center">
        {data.name}
      </Text>
      <Box mt="2em">
        <Flex
          justifyContent="center"
          alignItems="center"
          gap="10px"
          flexDir="column"
        >
          <Text textAlign="center" variant="heading-h3">
            {rupiah.format(data.price)}
          </Text>
          {data?.price_before_discount !== 0 && (
            <Flex gap="20px" alignItems="center">
              <Text
                textAlign="center"
                fontSize="14px"
                textDecoration="line-through"
                color="#1D2433"
              >
                {rupiah.format(data.price_before_discount)}
              </Text>
              <Flex bg="danger.700" minW="56px" justifyContent="center">
                <Text fontSize="12px" color="text.primary-white">
                  {data.discount_percent} %
                </Text>
              </Flex>
            </Flex>
          )}
        </Flex>

        <Button
          onClick={() => addToCart(data.id)}
          mt="10px"
          w="full"
          size="sm"
          colorScheme="blue"
        >
          Ambil Paket
        </Button>
      </Box>
      <Box mt="1.5em">
        <Text variant="body-medium-semibold" mb="0.5em">
          Fitur :
        </Text>
        <Flex gap="10px" alignItems="center" my="5px">
          <AiOutlineFileText />
          <Text variant="body-medium-medium">
            Pertanyaan: {data.total_question} item
          </Text>
        </Flex>
        <Flex gap="10px" alignItems="center" my="5px">
          <AiOutlineClockCircle />
          <Text variant="body-medium-medium">Durasi: {data.time} Menit</Text>
        </Flex>
        <Text variant="body-medium-semibold" my="0.5em">
          Sub Module :
        </Text>
        {data.sections.map((section) => {
          return (
            <Flex
              gap="10px"
              alignItems="center"
              key={`sections-${section.id}`}
              my="5px"
            >
              <AiFillCheckCircle color="#2F6FED" />
              <Text variant="body-medium-medium">{section.name}</Text>
            </Flex>
          );
        })}
      </Box>
    </Flex>
  );
};

export default PackageModal;
