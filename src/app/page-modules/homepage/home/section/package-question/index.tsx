import React, { useCallback, useContext, useState } from "react";
import { Box, Button, Flex, Grid, GridItem, Text } from "@chakra-ui/react";

import SampleTestCard from "@/simtest-components/cards/sample-test-card";
import {
  IQuestionPackageRes,
  TTestType,
} from "@/simtest-type/package/package.interface";
import {
  GlobalContext,
  IGlobalContext,
} from "@/simtest-context/global.context";
import PackageModal from "./components/PackageModal";
import SkeletonCard from "@/simtest-components/cards/skeleton-card";
import useCart from "@/simtest-hook/useCart";
import { useRouter } from "next/router";
import { NAVIGATION } from "@/simtest-type/navigation.interface";
import { EProductType } from "@/simtest-type/products/product.interface";

interface IPackageQuestionSectionProps {
  packageQuestion: IQuestionPackageRes[] | [];
  loading: boolean;
}

const PackageQuestionSection: React.FC<IPackageQuestionSectionProps> = ({
  packageQuestion,
  loading,
}) => {
  const router = useRouter();
  const { addToCart: addCartList } = useCart();
  const { modal, user } = useContext(GlobalContext) as IGlobalContext;
  const [filter, setFilter] = useState<EProductType | "all">("all");

  const filteredPackageQuestion = () => {
    if (filter === "all") {
      return packageQuestion;
    } else {
      return packageQuestion.filter((data) => data.package.name === filter);
    }
  };

  const addToCart = useCallback(
    (id: string) => {
      if (user?.profile?.token) {
        addCartList(id);
      } else {
        router.push(NAVIGATION.AUTH_LOGIN);
      }
    },
    [addCartList, router, user?.profile?.token]
  );

  const handleAction = useCallback(
    (isBought: boolean, data: IQuestionPackageRes) => {
      if (isBought) {
        router.push({
          pathname: NAVIGATION.DASHBOARD_MY_EXAM,
        });
      } else {
        modal?.onOpen();
        modal?.setContent({
          size: "md",
          closeButton: true,
          body: <PackageModal addToCart={(id) => addToCart(id)} data={data} />,
        });
      }
    },
    [addToCart, modal, router]
  );

  return (
    <Box
      as="section"
      w="full"
      py={{ base: "20px", lg: "40px" }}
      id="package-question"
    >
      <Text as="h1" variant="heading-h2">
        Pilihan Test Untukmu
      </Text>
      <Flex gap="10px" mt="20px">
        <Button
          borderRadius="20px"
          colorScheme="blue"
          size="sm"
          variant={filter === "all" ? "solid" : "outline"}
          fontSize="16px"
          onClick={() => setFilter("all")}
        >
          All Program
        </Button>
        <Button
          borderRadius="20px"
          colorScheme="blue"
          variant={filter === EProductType.BUMN ? "solid" : "outline"}
          size="sm"
          fontSize="16px"
          onClick={() => setFilter(EProductType.BUMN)}
        >
          BUMN
        </Button>
        <Button
          borderRadius="20px"
          colorScheme="blue"
          variant={filter === EProductType.LPDP ? "solid" : "outline"}
          size="sm"
          fontSize="16px"
          onClick={() => setFilter(EProductType.LPDP)}
        >
          LPDP
        </Button>
        <Button
          borderRadius="20px"
          colorScheme="blue"
          variant={filter === EProductType.CPNS ? "solid" : "outline"}
          size="sm"
          fontSize="16px"
          onClick={() => setFilter(EProductType.CPNS)}
        >
          CPNS
        </Button>
      </Flex>
      <Grid
        my="20px"
        templateColumns={{
          base: "repeat(1, 1fr)",
          sm: "repeat(2, 1fr)",
          lg: "repeat(3, 1fr)",
          xl: "repeat(4, 1fr)",
        }}
        gap="20px"
      >
        {loading && <SkeletonCard count={4} />}
        {packageQuestion.length &&
          filteredPackageQuestion().map((data) => {
            return (
              <GridItem key={data.id}>
                <SampleTestCard
                  moduleType={data.package.name as TTestType}
                  name={data.name}
                  duration={data.time}
                  quantity={data.total_question}
                  status={data.user_package_id > 0}
                  action={() => handleAction(data.user_package_id > 0, data)}
                />
              </GridItem>
            );
          })}
      </Grid>
    </Box>
  );
};

export default PackageQuestionSection;
