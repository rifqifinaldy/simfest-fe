import React from "react";
import { Flex, Box, Image, Text, Button } from "@chakra-ui/react";
import useRedirect from "@/simtest-hook/useRedirect/useRedirect";
import { NAVIGATION } from "@/simtest-type/navigation.interface";

const TryNowSection: React.FC = () => {
  const { guardRedirect } = useRedirect();

  return (
    <Flex
      as="section"
      alignItems="center"
      justifyContent="space-between"
      flexDir={{ base: "column", lg: "row" }}
      bg="#EEF4FA"
      px="20px"
      gap={{ base: "20px", lg: "80px" }}
      borderRadius="20px"
    >
      <Box w={{ base: "100%", lg: "40%" }} pl={{ base: "0", lg: "80px" }}>
        <Text
          variant="heading-h1"
          lineHeight={{ base: "38px", lg: "52.8px" }}
          fontSize={{ base: "32px", lg: "48px" }}
        >
          Masih Ragu Menghadapi Test? Coba Sekarang!
        </Text>
        <Text mt="1em" variant="body-large-regular">
          Melalui simulasi test, peroleh pengalaman menghadapi ujian dengan
          lebih mulus dan efisien. Disertai kumpulan pertanyaan beserta
          solusinya yang komprehensif
        </Text>
        <Button
          onClick={() => guardRedirect(NAVIGATION.DASHBOARD_PRODUCT)}
          mt="2em"
          colorScheme="blue"
          size="lg"
        >
          Latihan Sekarang
        </Button>
      </Box>
      <Box w={{ base: "100%", lg: "40%" }}>
        <Image
          w="100%"
          alt="simulasi test"
          src="/assets/homepage/course-illustration.svg"
        />
      </Box>
    </Flex>
  );
};

export default TryNowSection;
