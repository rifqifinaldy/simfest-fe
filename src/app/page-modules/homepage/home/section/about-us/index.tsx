import React, { useState } from "react";
import { Avatar, Box, Flex, SlideFade, Text } from "@chakra-ui/react";

const AboutUsSection: React.FC = () => {
  const [activeIndex, setActiveIndex] = useState(0);

  const testimonyContent = [
    {
      id: 0,
      author: "Rifqi Finaldy",
      message:
        "Simulasitest membantu persiapanku untuk ujian CPNS. Materi lengkap, pembahasan jelas, dan antarmuka yang simpel. Top deh!",
    },
    {
      id: 1,
      author: "Naufal Naufian",
      message:
        "Suka banget sama Simulasitest! Praktis, harganya pas di kantong, dan fitur ujiannya bener-bener mirip aslinya. Ngerasa siap deh buat ujian LPDP!",
    },
    {
      id: 2,
      author: "Moch. Fiqri",
      message:
        "Gak nyangka bisa dapetin platform sekomplit ini dengan harga se-oke ini. Latihan buat ujian BUMN jadi lebih asik",
    },
    {
      id: 3,
      author: "Dary",
      message:
        "CPNS, BUMN, LPDP, semuanya bisa latihan di Simulasitest. Gak rugi deh beli, beneran ngebantu banget",
    },
  ];
  return (
    <Flex
      as="section"
      flexDirection={{ base: "column", lg: "row" }}
      w={{ base: "calc(100% + 40px)", lg: "calc(100% + 160px)" }}
      py={{ base: "20px", lg: "80px" }}
      bg="#EEF4FA"
      mx={{ base: "-20px", lg: "-80px" }}
      px={{ base: "20px", lg: "80px" }}
      gap={{ base: "20px", lg: "80px" }}
      id="about-us"
      justifyContent={{ base: "flex-start", lg: "space-between" }}
    >
      <Box minW={{ base: "100%", lg: "30%" }}>
        <Text as="h2" variant="heading-h3" mb="0.5em">
          Kata Mereka Tentang Simulasitest
        </Text>
        <Text variant="body-large-regular">
          Simulasitest telah dipercaya lebih dari 1.000 siswa{" "}
        </Text>
      </Box>
      <Flex
        flexDir="column"
        justifyItems="flex-end"
        pl={{ base: "0", lg: "40px" }}
        w={{ base: "100%", lg: "70%" }}
      >
        {testimonyContent.map((testimoni) => {
          return (
            <SlideFade
              key={`testimoni-${testimoni.id}`}
              in={testimoni.id === activeIndex}
            >
              {testimoni.id === activeIndex && (
                <Box>
                  <Text
                    fontWeight="500"
                    fontSize={{ base: "18px", lg: "24px" }}
                    lineHeight={{ base: "24px", lg: "33.6px" }}
                    letterSpacing="0.2px"
                    textAlign="justify"
                    mb="1em"
                  >
                    {testimoni.message}
                  </Text>
                  <Flex alignItems="center" gap="10px">
                    <Avatar name={testimoni.author} size="md" />
                    <Box>
                      <Text variant="body-xlarge-medium" mb="0.25em">
                        {testimoni.author}
                      </Text>
                    </Box>
                  </Flex>
                </Box>
              )}
            </SlideFade>
          );
        })}
        <Flex justifyContent="flex-end" gap="20px">
          {testimonyContent.map((testimoni) => {
            return (
              <Box
                role="button"
                key={testimoni.id}
                w="16px"
                h="16px"
                rounded="full"
                bg="primary.900"
                opacity={testimoni.id === activeIndex ? "100%" : "20%"}
                onClick={() => setActiveIndex(testimoni.id)}
              />
            );
          })}
        </Flex>
      </Flex>
    </Flex>
  );
};

export default AboutUsSection;
