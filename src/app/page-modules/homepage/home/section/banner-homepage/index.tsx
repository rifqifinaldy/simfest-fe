import PillBadge from "@/simtest-components/badges/pill-badge";
import { testModuleDummy } from "@/simtest-dummy/test-module.dummy";
import useRedirect from "@/simtest-hook/useRedirect/useRedirect";
import { NAVIGATION } from "@/simtest-type/navigation.interface";
import {
  Avatar,
  AvatarGroup,
  Box,
  Button,
  Center,
  Flex,
  Grid,
  GridItem,
  Image,
  Text,
} from "@chakra-ui/react";
import React from "react";

const BannerHomepageSection: React.FC = () => {
  const { guardRedirect } = useRedirect();

  return (
    <Flex
      w={{ base: "calc(100% + 40px)", lg: "calc(100% + 160px)" }}
      as="section"
      mx={{ base: "-20px", lg: "-80px" }}
      px={{ base: "20px", lg: "80px" }}
      flexDirection={{ base: "column-reverse", md: "row" }}
      py="40px"
      bg="primary.700"
      position="relative"
      justifyContent="space-between"
      alignItems="center"
    >
      <Box w={{ base: "100%", md: "55%" }}>
        <Text
          mt={{ base: "0.5em", lg: "0" }}
          variant="heading-h2"
          color="text.primary-white"
        >
          Jadikanlah Dirimu yang Terpilih!
        </Text>
        <Text variant="body-large-regular" color="text.primary-white" mt="2em">
          <Text fontWeight="bold" as="span">
            Faktanya adalah 70% pendaftar LPDP gagal!, perbesar peluang kamu
            untuk terpilih mendapatkan beasiswa LPDP
          </Text>{" "}
          dengan menyiapkan tes skolastik LPDP. Dapatkan simulasi ujian terbaik
          dan koleksi soal terlengkap untuk mempersiapkan diri menghadapi tes
          masuk LPDP, BUMN, dan CPNS. Siapkan diri Anda untuk ujian selanjutnya
          dengan latihan soal skolastik LPDP. Selamat berlatih !
        </Text>
        <Grid
          my="3em"
          templateColumns={{ base: "repeat(3, 1fr)", md: "repeat(6, 1fr)" }}
          gap="15px"
        >
          {testModuleDummy.map((data) => {
            return (
              <GridItem key={data.id}>
                <PillBadge content={data.title} />
              </GridItem>
            );
          })}
        </Grid>
        <Flex
          gap="20px"
          alignItems="center"
          flexDir={{ base: "column", lg: "row" }}
        >
          <Button
            onClick={() => guardRedirect(NAVIGATION.DASHBOARD_PRODUCT)}
            fontWeight="700"
            color="black"
            colorScheme="yellow"
            w="200px"
            h="50px"
          >
            Latihan Sekarang
          </Button>
          <AvatarGroup size="sm" max={2}>
            <Avatar name="Ryan Florence" src="https://bit.ly/ryan-florence" />
            <Avatar name="Segun Adebayo" src="https://bit.ly/sage-adebayo" />
            <Avatar name="Kent Dodds" src="https://bit.ly/kent-c-dodds" />
            <Avatar
              name="Prosper Otemuyiwa"
              src="https://bit.ly/prosper-baba"
            />
            <Avatar name="Christian Nwamba" src="https://bit.ly/code-beast" />
          </AvatarGroup>
          <Text variant="body-medium-regular" color="text.primary-white">
            Bergabunglah dengan 1000+ Siswa
          </Text>
        </Flex>
      </Box>
      <Box w={{ base: "100%", md: "45%" }}>
        <Center>
          <Image
            alt="simulasi test"
            src="/assets/homepage/homepage-illustration.svg"
          />
        </Center>
      </Box>
    </Flex>
  );
};

export default BannerHomepageSection;
