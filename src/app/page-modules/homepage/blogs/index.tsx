import BlogsCard from "@/simtest-components/cards/blogs-card";
import { IBlog } from "@/simtest-type/blogs/blog.interface";
import { IThumbnail } from "@/simtest-type/contentful/contentful.interface";
import { Flex, Grid, GridItem, Text } from "@chakra-ui/react";
import { useRouter } from "next/router";
import React from "react";

interface BlogpageProps {
  blogs: IBlog[];
}

const Blogpage: React.FC<BlogpageProps> = ({ blogs }) => {
  const router = useRouter();

  return (
    <Flex flexDir="column" my="40px" p="20px">
      <Flex flexDir="column" pb="20px" borderBottom="1px solid #e1e6ef">
        <Text as="h1" variant="heading-h2">
          Our Blogs
        </Text>
        <Text as="h6" variant="body/sm" color="gray.300">
          simulasitestdotcom
        </Text>
      </Flex>
      <Grid
        gap="20px"
        my="40px"
        templateColumns={{
          base: "repeat(1, 1fr)",
          md: "repeat(2, 1fr)",
          "2xl": "repeat(4, 1fr)",
        }}
      >
        {blogs.map((blog) => {
          return (
            <GridItem
              onClick={() => router.push(`/blogs/${blog?.fields?.slug}`)}
              key={blog?.sys?.id}
            >
              <BlogsCard
                title={blog.fields.title as unknown as string}
                thumbnailUrl={
                  (blog?.fields?.thumbnail as unknown as IThumbnail).fields.file
                    .url
                }
                thumbnailCaption={
                  (blog?.fields?.thumbnail as unknown as IThumbnail).fields
                    ?.title as string
                }
                highlightText={blog?.fields?.highlightText as unknown as string}
                postedAt={blog.fields.postedAt as unknown as string}
              />
            </GridItem>
          );
        })}
      </Grid>
    </Flex>
  );
};

export default Blogpage;
