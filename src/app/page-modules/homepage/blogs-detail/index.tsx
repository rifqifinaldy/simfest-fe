import { BORDER, RADIUS } from "@/simtest-styles/style.constant";
import { IBlog } from "@/simtest-type/blogs/blog.interface";
import { IThumbnail } from "@/simtest-type/contentful/contentful.interface";
import { Box, Flex, Image, Text } from "@chakra-ui/react";
import dayjs from "dayjs";
import React from "react";
import Markdown from "react-markdown";

interface BlogDetailProps {
  blog: IBlog;
}

const BlogsDetail: React.FC<BlogDetailProps> = ({ blog }) => {
  return (
    <Box
      rounded="12px"
      p="40px"
      my="20px"
      bg="gray.50"
      shadow="sm"
      border={BORDER.DEFAULT}
    >
      <Flex
        flexDir={{ base: "column", md: "row" }}
        justifyContent="space-between"
      >
        <Text variant="heading-h1" as="h1" mb="14px">
          {blog.fields.title as unknown as string}
        </Text>
        <Box>
          <Text color="gray.400" fontWeight={500} fontSize="12px">
            Author: {blog.fields.author as unknown as string}
          </Text>
          <Text color="gray.400" fontWeight={500} fontSize="12px">
            {dayjs(blog.fields.postedAt as unknown as string).format(
              "DD MMMM YYYY"
            )}
          </Text>
        </Box>
      </Flex>
      <Flex
        py="20px"
        flexDir="column"
        gap="20px"
        justifyContent="center"
        alignItems="center"
      >
        <Image
          border={BORDER.DEFAULT}
          src={
            (blog.fields.bannerImage as unknown as IThumbnail)?.fields.file.url
          }
          alt={blog.fields.title as unknown as string}
          maxW={{ base: "full", "2xl": "1024px" }}
          rounded={RADIUS.XS}
          shadow="sm"
        />
        <Text
          fontSize="14px"
          color="gray.500"
          fontWeight={500}
          textAlign="center"
          as="h2"
        >
          {blog.fields.title as unknown as string}
        </Text>
      </Flex>

      <Markdown className="blogs">
        {blog.fields.content as unknown as string}
      </Markdown>
      <Flex gap="10px" mt="40px" flexWrap="wrap">
        {(blog.fields.tags as unknown as string[]).map((tag, i) => {
          return (
            <Text color="gray.400" fontWeight="500" key={i} as="h6">
              #{tag}
            </Text>
          );
        })}
      </Flex>
    </Box>
  );
};

export default BlogsDetail;
