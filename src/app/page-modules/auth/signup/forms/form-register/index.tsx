import useAuthentication from "@/simtest-hook/useAuthentication";
import {
  Box,
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
} from "@chakra-ui/react";
import { yupResolver } from "@hookform/resolvers/yup";
import React from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { IRegisterInput } from "type/auth/auth.interface";
import { validateRegister } from "./validation";

const FormRegister: React.FC = () => {
  const { signUp } = useAuthentication();
  const methods = useForm({
    resolver: yupResolver(validateRegister),
  });

  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = methods;

  const onSubmit: SubmitHandler<IRegisterInput> = (values) => {
    signUp(values);
  };

  return (
    <Box mt="1em">
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl isInvalid={Boolean(errors?.name)}>
          <FormLabel htmlFor="email">Nama</FormLabel>
          <Input
            id="name"
            placeholder="Nama Lengkap"
            type="text"
            {...register("name", {
              required: "This is required",
            })}
          />
          <FormErrorMessage>{errors?.name?.message}</FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={Boolean(errors?.email)}>
          <FormLabel htmlFor="email">Email</FormLabel>
          <Input
            id="email"
            placeholder="Email"
            type="email"
            {...register("email", {
              required: "This is required",
            })}
          />
          <FormErrorMessage>{errors?.email?.message}</FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={Boolean(errors?.password)}>
          <FormLabel htmlFor="name">Password</FormLabel>
          <Input
            id="password"
            placeholder="Password"
            type="password"
            {...register("password", {
              required: "This is required",
            })}
          />
          <FormErrorMessage>{errors?.password?.message}</FormErrorMessage>
        </FormControl>
        <Button
          mt="2em"
          w="full"
          colorScheme="blue"
          isLoading={isSubmitting}
          type="submit"
        >
          Daftar
        </Button>
      </form>
    </Box>
  );
};

export default FormRegister;
