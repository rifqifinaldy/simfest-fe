import * as Yup from "yup";

export const validateRegister = Yup.object().shape({
  name: Yup.string().required("Mohon cantumkan nama anda"),
  email: Yup.string().required("Mohon isi email anda"),
  password: Yup.string().required("Mohon isi password anda"),
});
