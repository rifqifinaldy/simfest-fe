import { Flex } from "@chakra-ui/react";
import React from "react";
import AuthHeader from "../components/auth-header";
import FormRegister from "./forms/form-register";
import AuthFooter from "../components/auth-footer";

const SignupPages: React.FC = () => {
  return (
    <Flex flexDir="column">
      <AuthHeader
        title="Daftar Akun"
        subtitle="Daftar dan mulai belajar bersama kami!"
      />
      <FormRegister />
      <AuthFooter />
    </Flex>
  );
};

export default SignupPages;
