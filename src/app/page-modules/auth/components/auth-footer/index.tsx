import useAuthentication from "@/simtest-hook/useAuthentication";
import { Button, Flex, Image, Text } from "@chakra-ui/react";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

const AuthFooter: React.FC = () => {
  const router = useRouter();

  const { googleAuth } = useAuthentication();

  return (
    <Flex flexDir="column" justifyContent="center" gap="20px">
      <Text textAlign="center" mt="20px" variant="body-large-regular">
        -- atau masuk dengan --
      </Text>
      <Button color="primary.700" onClick={() => googleAuth()}>
        <Image
          mr="1em"
          src="/assets/logo/logo-google.svg"
          alt="simulasi-test login with google"
        />
        Masuk dengan Google
      </Button>
      <Text textAlign="center" variant="body-large-regular">
        Belum punya akun?{" "}
        <Text
          as={Link}
          href={router.pathname === "/auth/register" ? "./login" : "./register"}
        >
          {router.pathname === "/auth/register" ? "Masuk" : "Daftar"}
        </Text>
      </Text>
    </Flex>
  );
};

export default AuthFooter;
