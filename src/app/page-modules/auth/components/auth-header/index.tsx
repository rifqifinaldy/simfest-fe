import { Flex, Text } from "@chakra-ui/react";
import React from "react";

interface AuthHeaderProps {
  title: string;
  subtitle?: string;
}

const AuthHeader: React.FC<AuthHeaderProps> = ({ title, subtitle }) => {
  return (
    <Flex flexDir="column">
      <Text variant="heading-h4">{title}</Text>
      {subtitle && <Text variant="body-large-regular">{subtitle}</Text>}
    </Flex>
  );
};

export default AuthHeader;
