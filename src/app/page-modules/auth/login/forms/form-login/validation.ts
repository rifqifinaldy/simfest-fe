import * as Yup from "yup";

export const validateLogin = Yup.object().shape({
  email: Yup.string().required("Mohon isi email anda"),
  password: Yup.string().required("Mohon isi password anda"),
});
