import {
  Box,
  Button,
  Checkbox,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Text,
} from "@chakra-ui/react";
import Link from "next/link";
import React from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { ILoginInput } from "type/auth/auth.interface";
import { validateLogin } from "./validation";
import { yupResolver } from "@hookform/resolvers/yup";
import useAuthentication from "@/simtest-hook/useAuthentication";

const FormLogin: React.FC = () => {
  const { login } = useAuthentication();
  const methods = useForm({
    resolver: yupResolver(validateLogin),
  });

  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = methods;

  const onSubmit: SubmitHandler<ILoginInput> = (values) => {
    login(values);
  };

  return (
    <Box mt="1em">
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl isInvalid={Boolean(errors?.email)}>
          <FormLabel htmlFor="email">Email</FormLabel>
          <Input
            id="email"
            placeholder="Email"
            type="email"
            {...register("email", {
              required: "This is required",
            })}
          />
          <FormErrorMessage>{errors?.email?.message}</FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={Boolean(errors?.password)}>
          <FormLabel htmlFor="name">Password</FormLabel>
          <Input
            id="password"
            placeholder="Password"
            type="password"
            {...register("password", {
              required: "This is required",
            })}
          />
          <FormErrorMessage>{errors?.password?.message}</FormErrorMessage>
        </FormControl>
        <Flex justifyContent="space-between">
          <Checkbox id="remember-me" name="remember-me">
            Ingat Saya
          </Checkbox>
          <Text as={Link} href="./forgot-password" variant="body-large-medium">
            Lupa Kata Sandi?
          </Text>
        </Flex>
        <Button
          mt="2em"
          w="full"
          colorScheme="blue"
          isLoading={isSubmitting}
          type="submit"
        >
          Masuk
        </Button>
      </form>
    </Box>
  );
};

export default FormLogin;
