import AuthHeader from "@/simtest-page-modules/auth/components/auth-header";
import { Flex } from "@chakra-ui/react";
import React from "react";
import FormLogin from "./forms/form-login";
import AuthFooter from "../components/auth-footer";

const LoginPage: React.FC = () => {
  return (
    <Flex flexDir="column">
      <AuthHeader
        title="Masuk Akun"
        subtitle="Masuk dan mulai belajar bersama kami"
      />
      <FormLogin />
      <AuthFooter />
    </Flex>
  );
};

export default LoginPage;
