import { Box, Flex, Text } from "@chakra-ui/react";
import React from "react";

interface ExamTimerProps {
  timer: {
    hours: number;
    minutes: number;
    seconds: number;
  };
}

const ExamTimer: React.FC<ExamTimerProps> = ({ timer }) => {
  return (
    <Flex
      w={{ base: "100%", lg: "33%" }}
      justifyContent={{ base: "center", lg: "flex-end" }}
    >
      <Box bg="white" p="20px" rounded="10px" shadow="sm">
        <Text variant={{ base: "heading-h6", lg: "heading-h3" }}>{`${
          timer.hours
        }:${String(timer.minutes).padStart(2, "0")}:${String(
          timer.seconds
        ).padStart(2, "0")}`}</Text>
      </Box>
    </Flex>
  );
};

export default ExamTimer;
