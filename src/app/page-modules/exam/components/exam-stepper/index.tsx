import { Flex, Progress, Text } from "@chakra-ui/react";
import React from "react";

interface ExamStepperProps {
  step: number;
}

const ExamStepper: React.FC<ExamStepperProps> = ({ step }) => {
  return (
    <Flex
      gap="10px"
      w={{ base: "100%", lg: "33%" }}
      flexDir="column"
      alignItems="center"
    >
      <Text variant="heading-h6">{step?.toFixed(0)}% Completed</Text>
      <Progress w="100%" size="lg" value={step} colorScheme="blue" />
    </Flex>
  );
};

export default ExamStepper;
