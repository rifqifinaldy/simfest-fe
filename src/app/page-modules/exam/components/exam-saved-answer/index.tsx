import {
  GlobalContext,
  IGlobalContext,
} from "@/simtest-context/global.context";
import { IInitialAnswer } from "@/simtest-hook/useExam";
import { EAnswerType } from "@/simtest-type/exam/exam-answer.interface";
import { Box, Button, Flex, Grid, GridItem, Text } from "@chakra-ui/react";
import { examAnswerColor } from "helper/exam/render-exam";
import React, { useCallback, useContext } from "react";

interface ExamSavedAnswerProps {
  answers: IInitialAnswer[];
  selectQuestion: (id: number) => void;
  activeIndex: number;
  submitExam: (payload: IInitialAnswer[]) => void;
  totalQuestion: number;
  totalAnswered: number;
}

const ExamSavedAnswer: React.FC<ExamSavedAnswerProps> = ({
  answers,
  selectQuestion,
  activeIndex,
  totalQuestion,
  totalAnswered,
  submitExam,
}) => {
  const { modal } = useContext(GlobalContext) as IGlobalContext;

  const finishExam = useCallback(() => {
    if (totalQuestion === totalAnswered) {
      submitExam(answers);
    } else {
      modal?.onOpen();
      modal?.setContent({
        size: "md",
        body: "Apakah kamu yakin, akan menyelesaikan soal ini? beberapa soal masih perlu untuk diselesaikan",
        overlayClose: false,
        footer: (
          <Flex justifyContent="space-between" gap="20px">
            <Button size="sm" colorScheme="red" onClick={() => modal.onClose()}>
              Batalkan
            </Button>
            <Button
              size="sm"
              colorScheme="green"
              onClick={() => submitExam(answers)}
            >
              Ya, Selesaikan Ujian Ini
            </Button>
          </Flex>
        ),
      });
    }
  }, [answers, modal, submitExam, totalAnswered, totalQuestion]);

  return (
    <>
      <Flex
        p="20px"
        bg="white"
        flexDir="column"
        alignItems={{ base: "flex-start", lg: "center" }}
        gap="20px"
      >
        <Text variant="heading-h4">Nomor Soal</Text>
        <Grid
          templateColumns={{
            base: "repeat(8, 1fr)",
            md: "repeat(12, 1fr)",
            xl: "repeat(5, 1fr)",
          }}
          gap="10px"
        >
          {answers.map((answer, i) => {
            return (
              <GridItem key={answer.id}>
                <Button
                  onClick={() => selectQuestion(answer.id)}
                  color="white"
                  bg={examAnswerColor(
                    activeIndex === i ? EAnswerType.ON_GOING : answer.status
                  )}
                  w="32px"
                  h="32px"
                  size={{ base: "sm", xl: "md" }}
                >
                  {i + 1}
                </Button>
              </GridItem>
            );
          })}
        </Grid>
        <Grid
          templateColumns={{ base: "repeat(4,1fr)", lg: "repeat(2, 1fr)" }}
          gap="10px"
        >
          <GridItem display="flex" alignItems="center" gap="10px">
            <Box w="15px" h="15px" bg="success.700" />
            <Text variant="body-small-regular">Terisi</Text>
          </GridItem>
          <GridItem display="flex" alignItems="center" gap="10px">
            <Box w="15px" h="15px" bg="#B6BEC2" />
            <Text variant="body-small-regular">Belum Terisi</Text>
          </GridItem>
          <GridItem display="flex" alignItems="center" gap="10px">
            <Box w="15px" h="15px" bg="secondary.700" />
            <Text variant="body-small-regular">Ragu</Text>
          </GridItem>
          <GridItem display="flex" alignItems="center" gap="10px">
            <Box w="15px" h="15px" bg="primary.700" />
            <Text variant="body-small-regular">Sedang Dikerjakan</Text>
          </GridItem>
        </Grid>
      </Flex>
      <Button
        colorScheme="blue"
        onClick={() => finishExam()}
        size={{ base: "sm", lg: "md" }}
      >
        Selesai
      </Button>
    </>
  );
};

export default ExamSavedAnswer;
