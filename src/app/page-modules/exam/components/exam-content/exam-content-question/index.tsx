import { IDiscussionQuestions } from "@/simtest-type/discussion/discussion.interface";
import { IExamQuestion } from "@/simtest-type/exam/exam-question.interface";
import {
  Box,
  Flex,
  Grid,
  GridItem,
  Image,
  Skeleton,
  Stack,
  Text,
} from "@chakra-ui/react";
import { MathJax, MathJaxContext } from "better-react-mathjax";
import React, { useEffect, useState } from "react";

interface ExamContentQuestionProps {
  activeIndex: number;
  activeQuestion: IExamQuestion | IDiscussionQuestions;
}

const ExamContentQuestion: React.FC<ExamContentQuestionProps> = ({
  activeIndex,
  activeQuestion,
}) => {
  const [renderDelay, setRenderDelay] = useState<boolean>(true);
  const config = {
    loader: { load: ["input/asciimath"] },
  };

  useEffect(() => {
    setRenderDelay(true);
    const delayTimeout = setTimeout(() => {
      setRenderDelay(false);
    }, 1000);
    return () => clearTimeout(delayTimeout);
  }, [activeQuestion?.id, activeQuestion?.question]);

  return (
    <MathJaxContext config={config}>
      <Flex flexDir="column">
        <Text variant="heading-h4" mb="20px">
          Soal No {activeIndex + 1}
        </Text>
        {(activeQuestion?.content_one || activeQuestion?.content_two) && (
          <Grid
            templateColumns={{ base: "repeat(1, 1fr)", md: "repeat(2, 1fr)" }}
            mb="20px"
          >
            {activeQuestion?.content_one && (
              <GridItem>
                <Image alt="question-1" src={activeQuestion.content_one} />
              </GridItem>
            )}
            {activeQuestion?.content_two && (
              <GridItem>
                <Image alt="question-1" src={activeQuestion.content_two} />
              </GridItem>
            )}
          </Grid>
        )}

        {renderDelay ? (
          <Stack>
            <Skeleton height="20px" />
            <Skeleton height="20px" />
          </Stack>
        ) : (
          <MathJax>
            {activeQuestion?.content_text && (
              <Box
                dangerouslySetInnerHTML={{
                  __html: activeQuestion?.content_text,
                }}
              />
            )}
            <Box
              dangerouslySetInnerHTML={{ __html: activeQuestion?.question }}
            />
          </MathJax>
        )}
      </Flex>
    </MathJaxContext>
  );
};

export default ExamContentQuestion;
