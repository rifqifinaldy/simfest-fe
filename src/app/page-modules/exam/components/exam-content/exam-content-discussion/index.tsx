import { IDiscussionQuestions } from "@/simtest-type/discussion/discussion.interface";
import {
  Box,
  Flex,
  Grid,
  GridItem,
  Image,
  Skeleton,
  Stack,
  Text,
} from "@chakra-ui/react";
import { MathJax, MathJaxContext } from "better-react-mathjax";
import React, { useEffect, useState } from "react";

interface IExamContentDiscussionProps {
  activeQuestion: IDiscussionQuestions;
}

const ExamContentDiscussion: React.FC<IExamContentDiscussionProps> = ({
  activeQuestion,
}) => {
  const [renderDelay, setRenderDelay] = useState<boolean>(true);
  const config = {
    loader: { load: ["input/asciimath"] },
  };

  useEffect(() => {
    setRenderDelay(true);
    const delayTimeout = setTimeout(() => {
      setRenderDelay(false);
    }, 1000);
    return () => clearTimeout(delayTimeout);
  }, [activeQuestion?.id, activeQuestion?.question]);

  return (
    <MathJaxContext config={config}>
      <Flex flexDir="column">
        <Text variant="heading-h4" my="20px">
          Pembahasan
        </Text>
        {activeQuestion?.discussion_content && (
          <Grid templateColumns="repeat(2, 1fr)" mb="20px">
            <GridItem>
              <Image
                alt={`simulasi-test-${activeQuestion?.id}question-1`}
                src={activeQuestion.discussion_content}
              />
            </GridItem>
          </Grid>
        )}
        {renderDelay ? (
          <Stack>
            <Skeleton height="20px" />
            <Skeleton height="20px" />
          </Stack>
        ) : (
          <MathJax>
            <Box
              dangerouslySetInnerHTML={{ __html: activeQuestion?.discussion }}
            />
          </MathJax>
        )}
      </Flex>
    </MathJaxContext>
  );
};

export default ExamContentDiscussion;
