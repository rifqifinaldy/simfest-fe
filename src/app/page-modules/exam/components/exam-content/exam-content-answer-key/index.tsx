import {
  IDiscussionQuestionOption,
  IDiscussionQuestions,
} from "@/simtest-type/discussion/discussion.interface";
import {
  Box,
  Button,
  Flex,
  RadioGroup,
  Skeleton,
  Stack,
} from "@chakra-ui/react";
import { MathJaxContext } from "better-react-mathjax";
import React, { useCallback, useEffect, useState } from "react";
import { AiOutlineArrowLeft, AiOutlineArrowRight } from "react-icons/ai";

interface ExamContentAnswerProps {
  questions: IDiscussionQuestions[];
  activeIndex: number;
  prevAnswer: () => void;
  nextAnswer: () => void;
  activeQuestion: IDiscussionQuestions;
}

const ExamContentAnswerKey: React.FC<ExamContentAnswerProps> = ({
  questions,
  activeIndex,
  prevAnswer,
  nextAnswer,
  activeQuestion,
}) => {
  const [renderDelay, setRenderDelay] = useState<boolean>(true);
  const renderAnswerColor = useCallback((option: IDiscussionQuestionOption) => {
    let text = "primary-black";
    let background = "transparent";
    let border = "1px solid #B6BEC2";
    let color = "#131313";

    if (option.is_answer) {
      text = "text.primary-white";
      background = "success.700";
      border = "none";
      color = "neutral.0";
    }
    if (option.is_chosen && !option.is_answer) {
      text = "#fff";
      background = "danger.700";
      border = "none";
      color = "neutral.0";
    }
    return { text, background, border, color };
  }, []);

  const config = {
    loader: { load: ["input/asciimath"] },
  };

  useEffect(() => {
    setRenderDelay(true);
    const delayTimeout = setTimeout(() => {
      setRenderDelay(false);
    }, 1000);
    return () => clearTimeout(delayTimeout);
  }, [activeQuestion?.id, activeQuestion?.question]);

  return (
    <Flex flexDir="column" gap="20px">
      <MathJaxContext config={config}>
        <Flex flexDir="column" gap="10px">
          <RadioGroup>
            <Stack direction="column">
              {activeQuestion?.option?.map((option, i) => {
                if (renderDelay) {
                  return (
                    <Stack key={`option-skeleton${i}`}>
                      <Skeleton height="50px" />
                    </Stack>
                  );
                }
                return (
                  <Box
                    key={`option-${i}`}
                    border={renderAnswerColor(option).border}
                    borderRadius="5px"
                    p="15px"
                    color={renderAnswerColor(option).color}
                    bg={renderAnswerColor(option).background}
                    dangerouslySetInnerHTML={{ __html: option?.value }}
                  />
                );
              })}
            </Stack>
          </RadioGroup>
        </Flex>
      </MathJaxContext>
      <Flex justifyContent="space-between" h="50px">
        <Flex gap="10px">
          <Button
            isDisabled={activeIndex === 0}
            colorScheme="blue"
            leftIcon={<AiOutlineArrowLeft />}
            size={{ base: "sm", xl: "lg" }}
            onClick={prevAnswer}
          >
            Sebelumnya
          </Button>
          <Button
            isDisabled={activeIndex === questions.length - 1}
            colorScheme="blue"
            rightIcon={<AiOutlineArrowRight />}
            size={{ base: "sm", xl: "lg" }}
            onClick={nextAnswer}
          >
            Selanjutnya
          </Button>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default ExamContentAnswerKey;
