import { IInitialAnswer } from "@/simtest-hook/useExam";
import { EAnswerType } from "@/simtest-type/exam/exam-answer.interface";
import { IExamQuestion } from "@/simtest-type/exam/exam-question.interface";
import {
  Box,
  Button,
  Flex,
  Radio,
  RadioGroup,
  Skeleton,
  Stack,
  Text,
} from "@chakra-ui/react";
import { MathJax, MathJaxContext } from "better-react-mathjax";
import React, { useEffect, useState } from "react";
import {
  AiFillFlag,
  AiOutlineArrowLeft,
  AiOutlineArrowRight,
} from "react-icons/ai";

interface ExamContentAnswerProps {
  handleAnswer?: (id: number, status: string, value: string) => void;
  questions: IExamQuestion[] | [];
  activeIndex: number;
  prevAnswer: () => void;
  nextAnswer: () => void;
  activeQuestion: IExamQuestion;
  answers: IInitialAnswer[];
}

const ExamContentAnswer: React.FC<ExamContentAnswerProps> = ({
  handleAnswer,
  questions,
  activeIndex,
  prevAnswer,
  nextAnswer,
  activeQuestion,
  answers,
}) => {
  const [renderDelay, setRenderDelay] = useState<boolean>(true);
  const config = {
    loader: { load: ["input/asciimath"] },
  };

  useEffect(() => {
    setRenderDelay(true);
    const delayTimeout = setTimeout(() => {
      setRenderDelay(false);
    }, 1000);
    return () => clearTimeout(delayTimeout);
  }, [activeQuestion?.id, activeQuestion?.question]);

  return (
    <Flex flexDir="column" gap="20px">
      <Text variant="heading-h4">Jawab :</Text>
      <MathJaxContext config={config}>
        <Flex flexDir="column" gap="10px">
          <RadioGroup
            onChange={(value) => {
              if (handleAnswer) {
                handleAnswer(activeQuestion.id, EAnswerType.ANSWERED, value);
              }
            }}
            value={answers[activeIndex]?.value}
          >
            <Stack direction="column">
              {activeQuestion?.option?.map((option, i) => {
                if (renderDelay) {
                  return (
                    <Stack key={`option-skeleton${i}`}>
                      <Skeleton height="50px" />
                    </Stack>
                  );
                }
                return (
                  <Box
                    onClick={() => {
                      if (handleAnswer) {
                        handleAnswer(
                          activeQuestion.id,
                          EAnswerType.ANSWERED,
                          option.value
                        );
                      }
                    }}
                    cursor="pointer"
                    key={`option-${i}`}
                    border={`1px solid ${
                      answers[activeIndex]?.value === "1"
                        ? "#2F6FED"
                        : "#B6BEC2"
                    }`}
                    borderRadius="5px"
                    bg={
                      answers[activeIndex]?.value === option.value
                        ? "#F0F5FF"
                        : "transparent"
                    }
                  >
                    <Radio
                      p={{ base: "10px", lg: "20px" }}
                      value={option.value}
                    >
                      <MathJax>
                        <Text
                          variant={{
                            base: "body-medium-medium",
                            lg: "body-xlarge-medium",
                          }}
                          color={`${
                            answers[activeIndex]?.value === option.value
                              ? "#2F6FED"
                              : ""
                          }`}
                          dangerouslySetInnerHTML={{ __html: option?.value }}
                        />
                      </MathJax>
                    </Radio>
                  </Box>
                );
              })}
            </Stack>
          </RadioGroup>
        </Flex>
      </MathJaxContext>
      <Flex
        justifyContent="space-between"
        gap="20px"
        flexDir={{ base: "column", sm: "row" }}
      >
        <Flex gap="10px">
          <Button
            isDisabled={activeIndex === 0}
            colorScheme="blue"
            leftIcon={<AiOutlineArrowLeft />}
            size={{ base: "sm", xl: "lg" }}
            onClick={prevAnswer}
          >
            Sebelumnya
          </Button>
          <Button
            isDisabled={activeIndex === questions.length - 1}
            colorScheme="blue"
            rightIcon={<AiOutlineArrowRight />}
            size={{ base: "sm", xl: "lg" }}
            onClick={nextAnswer}
          >
            Selanjutnya
          </Button>
        </Flex>
        <Button
          colorScheme="yellow"
          color="white"
          leftIcon={<AiFillFlag />}
          size={{ base: "sm", xl: "lg" }}
          onClick={() => {
            if (handleAnswer) {
              handleAnswer(activeQuestion.id, EAnswerType.HESISTANT, "");
            }
          }}
        >
          Ragu
        </Button>
      </Flex>
    </Flex>
  );
};

export default ExamContentAnswer;
