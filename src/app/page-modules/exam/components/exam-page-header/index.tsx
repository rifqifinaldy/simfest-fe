import { Flex, Text } from "@chakra-ui/react";
import React from "react";

interface ExamPageHeaderProps {
  title: string;
  subtitle?: string;
}

const ExamPageHeader: React.FC<ExamPageHeaderProps> = ({ title, subtitle }) => {
  return (
    <Flex flexDir="column" gap="10px" w={{ base: "100%", lg: "33%" }}>
      <Text variant="heading-h3">{title}</Text>
      <Text variant="body-medium-medium">{subtitle}</Text>
    </Flex>
  );
};

export default ExamPageHeader;
