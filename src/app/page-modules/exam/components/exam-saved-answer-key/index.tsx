import {
  IDiscussionQuestionOption,
  IDiscussionQuestions,
} from "@/simtest-type/discussion/discussion.interface";
import { NAVIGATION } from "@/simtest-type/navigation.interface";
import { Box, Button, Flex, Grid, GridItem, Text } from "@chakra-ui/react";
import { useRouter } from "next/router";
import React, { useCallback } from "react";

interface ExamSavedAnswerProps {
  questions: IDiscussionQuestions[];
  selectQuestion: (id: number) => void;
  activeIndex: number;
}

const ExamSavedAnswerKey: React.FC<ExamSavedAnswerProps> = ({
  questions,
  selectQuestion,
  activeIndex,
}) => {
  const router = useRouter();

  const renderAnswerColor = useCallback(
    (options: IDiscussionQuestionOption[]) => {
      let background = "danger.700";
      let text = "white";
      options.map((option) => {
        if (option.is_answer && option.is_chosen) {
          background = "success.700";
          text = "white";
        }
        if (option.is_chosen && !option.is_answer) {
          background = "danger.700";
          text = "white";
        }
      });
      return { background, text };
    },
    []
  );
  return (
    <>
      <Flex
        p="20px"
        bg="white"
        flexDir="column"
        alignItems={{ base: "flex-start", sm: "center" }}
        gap="20px"
      >
        <Text variant="heading-h4">Nomor Soal</Text>
        <Grid
          templateColumns={{
            base: "repeat(8, 1fr)",

            md: "repeat(12, 1fr)",
            xl: "repeat(5, 1fr)",
          }}
          gap="10px"
        >
          {questions.map((question, i) => {
            return (
              <GridItem key={i}>
                <Button
                  border="1px solid #B6BEC2"
                  onClick={() => selectQuestion(question.id)}
                  color={renderAnswerColor(question.option).text}
                  bg={renderAnswerColor(question.option).background}
                  w="32px"
                  h="32px"
                  size={{ base: "sm", xl: "md" }}
                  opacity={activeIndex === i ? 0.5 : 1}
                >
                  {i + 1}
                </Button>
              </GridItem>
            );
          })}
        </Grid>
        <Grid templateColumns="repeat(2, 1fr)" gap="10px">
          <GridItem display="flex" alignItems="center" gap="10px">
            <Box w="15px" h="15px" bg="success.700" />
            <Text variant="body-small-regular">Benar</Text>
          </GridItem>
          <GridItem display="flex" alignItems="center" gap="10px">
            <Box w="15px" h="15px" bg="danger.700" />
            <Text variant="body-small-regular">Salah / Tidak Terisi</Text>
          </GridItem>
        </Grid>
      </Flex>
      <Button
        colorScheme="blue"
        onClick={() => router.push(NAVIGATION.DASHBOARD_MY_EXAM)}
        size={{ base: "sm", lg: "md" }}
      >
        Selesai Pembahasan
      </Button>
    </>
  );
};

export default ExamSavedAnswerKey;
