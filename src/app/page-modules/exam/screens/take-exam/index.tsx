import { Box, Flex } from "@chakra-ui/react";
import React, { useContext } from "react";
import ExamPageHeader from "../../components/exam-page-header";
import ExamStepper from "../../components/exam-stepper";
import ExamTimer from "../../components/exam-timer";
import ExamSavedAnswer from "../../components/exam-saved-answer";
import { ExamContext, IExamContext } from "@/simtest-context/exam.context";
import ExamContentAnswer from "../../components/exam-content/exam-content-answer";
import ExamContentQuestion from "../../components/exam-content/exam-content-question";
import { useRouter } from "next/router";

const TakeExamPages: React.FC = () => {
  const {
    handleAnswer,
    questions,
    answers,
    selectQuestion,
    activeIndex,
    submitExam,
    prevAnswer,
    nextAnswer,
    activeQuestion,
    step,
    timer,
  } = useContext(ExamContext) as IExamContext;

  const router = useRouter();

  const ExamSavedAnswerProps = {
    answers,
    selectQuestion,
    activeIndex,
    submitExam,
    totalQuestion: questions.length,
    totalAnswered: answers.filter((answer) => answer.status === "ANSWERED")
      .length,
  };

  const ExamContentAnswerProps = {
    handleAnswer,
    questions,
    activeIndex,
    prevAnswer,
    nextAnswer,
    activeQuestion,
    answers,
  };

  const ExamContenQuestionProps = {
    activeIndex,
    activeQuestion,
  };

  return (
    <Box>
      <Flex
        mb={{ base: "20px", lg: "40px" }}
        alignItems={{ base: "flex-start", lg: "center" }}
        justifyContent="space-between"
        gap="20px"
        flexDir={{ base: "column", lg: "row" }}
      >
        <ExamPageHeader
          title={`${router?.query?.p} ${router?.query?.n}`}
          subtitle="Kerjakan soal untuk mengukur kemampuan anda"
        />
        <ExamStepper step={step} />
        <ExamTimer timer={timer} />
      </Flex>
      <Flex
        gap="20px"
        justifyContent="space-between"
        alignItems="flex-start"
        flexDir={{ base: "column", xl: "row" }}
      >
        <Box
          w={{ base: "100%", xl: "80%" }}
          shadow="sm"
          borderRadius="8px"
          bg="white"
          p={{ base: "20px", lg: "40px" }}
        >
          <Flex flexDir="column" gap="20px">
            <ExamContentQuestion {...ExamContenQuestionProps} />
            <ExamContentAnswer {...ExamContentAnswerProps} />
          </Flex>
        </Box>
        <Flex
          w={{ base: "100%", xl: "20%" }}
          gap="20px"
          justifyContent="center"
          flexDir="column"
        >
          <ExamSavedAnswer {...ExamSavedAnswerProps} />
        </Flex>
      </Flex>
    </Box>
  );
};

export default TakeExamPages;
