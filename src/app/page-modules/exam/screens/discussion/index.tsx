import { Box, Flex, Text } from "@chakra-ui/react";
import React, { useContext } from "react";
import ExamPageHeader from "../../components/exam-page-header";
import {
  DiscussionContext,
  IDiscussionContext,
} from "@/simtest-context/discussion.context";
import ExamContentQuestion from "../../components/exam-content/exam-content-question";
import ExamContentAnswerKey from "../../components/exam-content/exam-content-answer-key";
import ExamSavedAnswerKey from "../../components/exam-saved-answer-key";
import ExamContentDiscussion from "../../components/exam-content/exam-content-discussion";
import { useRouter } from "next/router";
import { NAVIGATION } from "@/simtest-type/navigation.interface";
import { AiOutlineArrowLeft } from "react-icons/ai";

const DiscussionPages: React.FC = () => {
  const router = useRouter();

  const {
    discussion: questions,
    activeIndex,
    activeQuestion,
    prevAnswer,
    nextAnswer,
    selectQuestion,
  } = useContext(DiscussionContext) as IDiscussionContext;

  const ExamContenQuestionProps = {
    activeIndex,
    activeQuestion,
  };

  const ExamContentAnswerKeyProps = {
    questions,
    activeIndex,
    prevAnswer,
    nextAnswer,
    activeQuestion,
  };

  const ExamSavedAnswerKeyProps = {
    questions,
    activeIndex,
    selectQuestion,
  };

  return (
    <Box>
      <Flex
        cursor="pointer"
        alignItems="center"
        mb="10px"
        onClick={() => router.push(NAVIGATION.DASHBOARD_MY_EXAM)}
      >
        <AiOutlineArrowLeft />
        <Text ml="20px">Kembali Ke Dashboard</Text>
      </Flex>
      <Flex
        mb={{ base: "20px", lg: "40px" }}
        alignItems={{ base: "flex-start", lg: "center" }}
        justifyContent="space-between"
        gap="20px"
        flexDir={{ base: "column", lg: "row" }}
      >
        <ExamPageHeader
          title={`${router?.query?.p} ${router?.query?.n}`}
          subtitle="Pembahasan Masalah"
        />
      </Flex>
      <Flex
        gap="20px"
        justifyContent="space-between"
        alignItems="flex-start"
        flexDir={{ base: "column", xl: "row" }}
      >
        <Box
          w={{ base: "100%", xl: "80%" }}
          shadow="sm"
          borderRadius="8px"
          bg="white"
          p={{ base: "20px", lg: "40px" }}
        >
          <Flex flexDir="column" gap="20px">
            <ExamContentQuestion {...ExamContenQuestionProps} />
            <ExamContentAnswerKey {...ExamContentAnswerKeyProps} />
            <ExamContentDiscussion activeQuestion={activeQuestion} />
          </Flex>
        </Box>
        <Flex
          w={{ base: "100%", xl: "20%" }}
          gap="20px"
          justifyContent="center"
          flexDir="column"
        >
          <ExamSavedAnswerKey {...ExamSavedAnswerKeyProps} />
        </Flex>
      </Flex>
    </Box>
  );
};

export default DiscussionPages;
