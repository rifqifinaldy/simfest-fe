import {
  EXAM_SELECTOR,
  REQUEST_SUBMIT_EXAM,
  STORE_TAKE_EXAM,
} from "@/simtest-redux/exam";
import { useAppDispatch, useAppSelector } from "@/simtest-redux/useRedux";
import {
  EAnswerType,
  IExamSubmit,
} from "@/simtest-type/exam/exam-answer.interface";
import {
  IExamQuestion,
  IExamResponse,
} from "@/simtest-type/exam/exam-question.interface";
import { useRouter } from "next/router";
import { useCallback, useContext, useEffect, useState } from "react";
import useResponse from "../useResponses";
import { NAVIGATION } from "@/simtest-type/navigation.interface";
import { Button, Flex } from "@chakra-ui/react";
import {
  GlobalContext,
  IGlobalContext,
} from "@/simtest-context/global.context";

export interface IInitialAnswer {
  id: number;
  status: string;
  value: string;
}

interface ISavedItems {
  id: string | string[] | undefined;
  answers: IInitialAnswer[];
  activeQuestion: IExamQuestion;
  activeIndex: number;
  step: number;
  remainingTime: number;
  submitExam: (payload: IInitialAnswer[]) => void;
}

const initialAnswer = [
  {
    id: 0,
    status: EAnswerType.NULL,
    value: "",
  },
];

const useExam = () => {
  const { modal } = useContext(GlobalContext) as IGlobalContext;
  const router = useRouter();
  const dispatch = useAppDispatch();
  const {
    examQuestions: questions,
    pending,
    error,
  } = useAppSelector(EXAM_SELECTOR);
  const { handleSuccess, handleError } = useResponse();

  const [answers, setAnswers] = useState<IInitialAnswer[]>(initialAnswer);
  const [activeIndex, setActiveIndex] = useState<number>(0);
  const [activeQuestion, setActiveQuestion] = useState<IExamQuestion>(
    questions[0]
  );
  const [step, setStep] = useState<number>(0);
  const initialTimeInSeconds = parseInt(router?.query?.t as string) * 60;
  const [remainingTime, setRemainingTime] = useState(initialTimeInSeconds);

  const storeTakeExam = useCallback(
    (data: IExamResponse) => {
      dispatch(STORE_TAKE_EXAM({ data }));
    },
    [dispatch]
  );

  const nextAnswer = useCallback(() => {
    if (activeIndex !== questions.length - 1) {
      setActiveIndex((activeIndex) => activeIndex + 1);
    }
  }, [activeIndex, questions.length]);

  const prevAnswer = useCallback(() => {
    setActiveIndex((activeIndex) => activeIndex - 1);
  }, []);

  const selectQuestion = useCallback(
    (id: number) => {
      const tmpIndex = questions.findIndex((question) => question.id === id);
      setActiveIndex(tmpIndex);
    },
    [questions]
  );

  const handleAnswer = useCallback(
    (id: number, status: string, value: string) => {
      if (status === EAnswerType.HESISTANT) {
        nextAnswer();
      }
      const tmpIndex = answers.findIndex((answer) => answer.id === id);
      const tmpAnswer = [...answers];
      tmpAnswer[tmpIndex] = {
        id,
        status,
        value,
      };
      setAnswers(tmpAnswer);
    },
    [answers, nextAnswer]
  );

  const calculatePercentage = useCallback(() => {
    const answeredCount = answers.filter(
      (item) => item.status === "ANSWERED"
    ).length;
    const totalCount = answers.length;

    if (totalCount === 0) {
      setStep(0);
    }
    const result = (answeredCount / totalCount) * 100;
    setStep(result);
  }, [answers]);

  const saveToLocalStorage = useCallback(() => {
    const savedItems = {
      id: router?.query?.id,
      answers: answers,
      activeQuestion: activeQuestion,
      activeIndex: activeIndex,
      step: step,
      remainingTime: remainingTime,
    };
    localStorage.setItem(
      router?.query?.id as string,
      JSON.stringify(savedItems)
    );
  }, [
    activeIndex,
    activeQuestion,
    answers,
    remainingTime,
    router?.query?.id,
    step,
  ]);

  const restoreFromLocalStorage = useCallback(
    (savedItems: ISavedItems) => {
      if (savedItems.id === router?.query?.id) {
        setActiveIndex(savedItems.activeIndex);
        setActiveQuestion(savedItems.activeQuestion);
        setRemainingTime(savedItems.remainingTime);
        setAnswers(savedItems.answers);
        setStep(savedItems.step);
      }
    },
    [router?.query?.id]
  );

  const checkLocalStorage = useCallback(() => {
    const savedItemsString = localStorage.getItem(router?.query?.id as string);
    if (savedItemsString) {
      const savedItems = JSON.parse(savedItemsString);
      restoreFromLocalStorage(savedItems);
    } else {
      const tmpAnswers: IInitialAnswer[] = [];
      questions.map((question) => {
        return tmpAnswers.push({
          id: question.id,
          status: EAnswerType.NOT_ANSWERED as string,
          value: "",
        });
      });
      setAnswers(() => tmpAnswers);
    }
  }, [questions, restoreFromLocalStorage, router?.query?.id]);

  const submitExam = useCallback(
    (payload: IInitialAnswer[]) => {
      const convertQuestionAnswer = payload.map((data) => {
        return {
          question_id: data.id,
          answer: data.value,
        };
      });
      const body: IExamSubmit = {
        user_section_id: router?.query?.id as string,
        time_spent: Math.trunc(
          parseInt(router?.query?.t as string) - remainingTime / 60
        ),
        exam_question_answer: convertQuestionAnswer,
      };
      dispatch(REQUEST_SUBMIT_EXAM(body)).then((result) => {
        if (result.meta.requestStatus === "fulfilled") {
          handleSuccess("Selamat, anda telah menyelesaikan paket soal ini");
          modal?.onOpen();
          modal?.setContent({
            size: "md",
            body: "Selamat anda telah menyelesaikan paket soal ini",
            overlayClose: false,
            footer: (
              <Flex justifyContent="space-between" gap="20px">
                <Button
                  size="sm"
                  colorScheme="blue"
                  onClick={() => {
                    localStorage.removeItem(router?.query?.id as string);
                    router.push(NAVIGATION.DASHBOARD_MY_EXAM);
                    modal?.onClose();
                  }}
                >
                  Kembali Ke Dashboard
                </Button>
                <Button
                  size="sm"
                  colorScheme="green"
                  onClick={() => {
                    localStorage.removeItem(router?.query?.id as string);
                    router.push(
                      `${NAVIGATION.DISCUSSION}/${result.payload.data.id}`
                    );
                    router.push({
                      pathname: `${NAVIGATION.DISCUSSION}/${result.payload.data.id}`,
                      query: { n: router?.query?.n, p: router?.query?.p },
                    });
                    modal?.onClose();
                  }}
                >
                  Bahas Paket Soal Ini
                </Button>
              </Flex>
            ),
          });
        } else {
          handleError(
            result?.payload?.response?.status,
            result?.payload?.response?.data?.msg?.id
          );
        }
      });
    },
    [dispatch, handleError, handleSuccess, modal, remainingTime, router]
  );

  useEffect(() => {
    if (questions.length > 0) {
      setActiveQuestion(questions[activeIndex]);
      calculatePercentage();
    }
  }, [activeIndex, calculatePercentage, questions]);

  const alertUser = useCallback(
    (e: Event) => {
      e.preventDefault();
      saveToLocalStorage();
    },
    [saveToLocalStorage]
  );

  useEffect(() => {
    window.addEventListener("beforeunload", alertUser);
    return () => {
      window.removeEventListener("beforeunload", alertUser);
    };
  }, [alertUser]);

  useEffect(() => {
    router.beforePopState(({ as }) => {
      if (as !== router.asPath) {
        saveToLocalStorage();
      }
      return true;
    });

    return () => {
      router.beforePopState(() => true);
    };
  }, [router, saveToLocalStorage]);

  useEffect(() => {
    checkLocalStorage();
  }, [checkLocalStorage]);

  useEffect(() => {
    const interval = setInterval(() => {
      setRemainingTime((prevTime) => {
        if (prevTime > 0) {
          return prevTime - 1;
        } else {
          clearInterval(interval);
          return 0;
        }
      });
    }, 1000);
    return () => clearInterval(interval);
  }, [initialTimeInSeconds]);

  // Convert remaining time to hours, minutes, and seconds
  const hours = Math.floor(remainingTime / 3600);
  const minutes = Math.floor((remainingTime % 3600) / 60);
  const seconds = remainingTime % 60;
  const timer = {
    hours,
    minutes,
    seconds,
  };

  return {
    submitExam,
    activeIndex,
    storeTakeExam,
    pending,
    error,
    questions,
    activeQuestion,
    setActiveQuestion,
    answers,
    selectQuestion,
    nextAnswer,
    prevAnswer,
    handleAnswer,
    step,
    timer,
  };
};

export default useExam;
