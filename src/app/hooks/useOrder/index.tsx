import {
  ORDER_SELECTOR_COLLECTION,
  REQUEST_ORDER,
  REQUEST_ORDER_HISTORY,
} from "@/simtest-redux/order";
import useResponse from "../useResponses";
import { useAppDispatch, useAppSelector } from "@/simtest-redux/useRedux";
import { useCallback, useContext } from "react";
import { useRouter } from "next/router";
import { NAVIGATION } from "@/simtest-type/navigation.interface";
import {
  GlobalContext,
  IGlobalContext,
} from "@/simtest-context/global.context";

const useOrder = () => {
  const { drawer } = useContext(GlobalContext) as IGlobalContext;
  const router = useRouter();
  const dispatch = useAppDispatch();
  const { handleSuccess, handleError } = useResponse();
  const { order, orderList, pending, success, error, refetch } = useAppSelector(
    ORDER_SELECTOR_COLLECTION
  );

  const createOrder = useCallback(
    (voucher: string) => {
      dispatch(REQUEST_ORDER(voucher)).then((result) => {
        if (result.meta.requestStatus === "fulfilled") {
          handleSuccess("Berhasil Terbeli");
          if (result?.payload?.data?.token) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (window as any).snap.pay(result.payload.data.token, {
              onSuccess: () => {
                router.push(`${NAVIGATION.DASHBOARD_MY_EXAM}`);
                drawer?.onClose();
              },
              onPending: () => {
                router.push(`${NAVIGATION.DASHBOARD_TRANSACTION}`);
                drawer?.onClose();
              },
              onError: () => {
                alert("payment failed!");
                router.push(`${NAVIGATION.DASHBOARD_TRANSACTION}`);
                drawer?.onClose();
              },
              onClose: () => {
                router.push(`${NAVIGATION.DASHBOARD_TRANSACTION}`);
                drawer?.onClose();
              },
            });
          } else {
            router.push(`${NAVIGATION.DASHBOARD_MY_EXAM}`);
            drawer?.onClose();
          }
        } else {
          handleError(
            result?.payload?.response?.status,
            result?.payload?.response?.data?.msg?.id
          );
        }
      });
    },
    [dispatch, drawer, handleError, handleSuccess, router]
  );

  const getOrderList = useCallback(() => {
    dispatch(REQUEST_ORDER_HISTORY());
  }, [dispatch]);

  return {
    createOrder,
    getOrderList,
    order,
    orderList,
    pending,
    success,
    error,
    refetch,
  };
};

export default useOrder;
