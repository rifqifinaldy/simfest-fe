import { IAuthResponse } from "@/simtest-type/auth/auth.interface";
import Cookies from "js-cookie";
import { useCallback, useEffect, useState } from "react";

const useProfile = () => {
  const [profile, setProfile] = useState<IAuthResponse | null>(null);

  useEffect(() => {
    const tmpUser = Cookies.get("usr_dt");
    const st = Cookies.get("st");
    if (tmpUser && st) {
      setProfile(JSON.parse(tmpUser));
    }
  }, []);

  const updateProfile = useCallback((data: IAuthResponse) => {
    const tmpUser = JSON.stringify({
      email: data.email,
      name: data.name,
      token: data.token,
      chartCount: 0,
    });
    Cookies.set("usr_dt", tmpUser);
    Cookies.set("st", data.token);
    setProfile(data);
  }, []);

  return { profile, updateProfile };
};

export default useProfile;
