import { useAppDispatch, useAppSelector } from "@/simtest-redux/useRedux";
import { useCallback, useEffect, useState } from "react";
import {
  DISCUSSION_SELECTOR,
  STORE_DISCUSSION,
} from "@/simtest-redux/discussion";
import {
  IDiscussionQuestions,
  IDiscussionResponse,
} from "@/simtest-type/discussion/discussion.interface";

const useDiscussion = () => {
  const dispatch = useAppDispatch();
  const { pending, error, success, discussion } =
    useAppSelector(DISCUSSION_SELECTOR);

  const [activeIndex, setActiveIndex] = useState<number>(0);
  const [activeQuestion, setActiveQuestion] = useState<IDiscussionQuestions>(
    discussion[0]
  );

  const storeDiscussion = useCallback(
    (data: IDiscussionResponse) => {
      dispatch(STORE_DISCUSSION({ data }));
    },
    [dispatch]
  );

  const nextAnswer = useCallback(() => {
    if (activeIndex !== discussion.length - 1) {
      setActiveIndex((activeIndex) => activeIndex + 1);
    }
  }, [activeIndex, discussion.length]);

  const prevAnswer = useCallback(() => {
    setActiveIndex((activeIndex) => activeIndex - 1);
  }, []);

  const selectQuestion = useCallback(
    (id: number) => {
      const tmpIndex = discussion.findIndex((question) => question.id === id);
      setActiveIndex(tmpIndex);
    },
    [discussion]
  );

  useEffect(() => {
    if (discussion.length > 0) {
      setActiveQuestion(discussion[activeIndex]);
    }
  }, [activeIndex, discussion]);

  return {
    storeDiscussion,
    pending,
    error,
    success,
    discussion,
    activeIndex,
    activeQuestion,
    nextAnswer,
    prevAnswer,
    selectQuestion,
  };
};

export default useDiscussion;
