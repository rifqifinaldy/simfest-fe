import {
  GlobalContext,
  IGlobalContext,
} from "@/simtest-context/global.context";
import { useRouter } from "next/router";
import { useCallback, useContext } from "react";

const useRedirect = () => {
  const { user } = useContext(GlobalContext) as IGlobalContext;
  const router = useRouter();

  const guardRedirect = useCallback(
    (path: string) => {
      if (user?.profile?.token) {
        router.push(path);
      } else {
        router.push(path);
      }
    },
    [router, user?.profile?.token]
  );

  return { guardRedirect };
};

export default useRedirect;
