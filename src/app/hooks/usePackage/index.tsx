import { useAppDispatch, useAppSelector } from "@/simtest-redux/useRedux";
import { useCallback } from "react";
import {
  PACKAGE_SELECTOR_COLLECTION,
  REQUEST_MY_PACKAGE,
  REQUEST_PACKAGE_HOMEPAGE,
  REQUEST_PACKAGE_LIST,
} from "@/simtest-redux/package-question";
import { IProductGetParams } from "@/simtest-type/products/product.interface";

const usePackage = () => {
  const dispatch = useAppDispatch();
  const {
    homepagePackage,
    success,
    myPackage,
    packageList,
    pending,
    error,
    packageListMeta,
  } = useAppSelector(PACKAGE_SELECTOR_COLLECTION);

  const getPackageList = useCallback(
    (params: IProductGetParams) => {
      dispatch(REQUEST_PACKAGE_LIST(params));
    },
    [dispatch]
  );

  const getMyPackage = useCallback(
    (params: IProductGetParams) => {
      dispatch(REQUEST_MY_PACKAGE(params));
    },
    [dispatch]
  );

  const getHomepagePackage = useCallback(() => {
    dispatch(REQUEST_PACKAGE_HOMEPAGE());
  }, [dispatch]);

  return {
    myPackage,
    packageList,
    pending,
    error,
    homepagePackage,
    success,
    getPackageList,
    getMyPackage,
    getHomepagePackage,
    packageListMeta,
  };
};

export default usePackage;
