import {
  AUTH_SELECTOR_COLLECTION,
  REQUEST_REGISTER,
  REQUEST_SIGN_IN,
  REQUEST_SIGN_IN_GOOGLE,
} from "@/simtest-redux/authentication";
import { useAppDispatch, useAppSelector } from "@/simtest-redux/useRedux";
import { useCallback } from "react";
import { ILoginInput, IRegisterInput } from "type/auth/auth.interface";
import Cookies from "js-cookie";
import crypto from "crypto";
import useResponse from "../useResponses";
import { useRouter } from "next/router";
import useProfile from "../useProfile";
import axios from "axios";
import { NAVIGATION } from "@/simtest-type/navigation.interface";

const useAuthentication = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const { handleError, handleSuccess } = useResponse();
  const { data, pending, error } = useAppSelector(AUTH_SELECTOR_COLLECTION);
  const { updateProfile } = useProfile();

  const login = useCallback(
    (payload: ILoginInput) => {
      dispatch(REQUEST_SIGN_IN(payload)).then((result) => {
        if (result.meta.requestStatus === "fulfilled") {
          handleSuccess("berhasil login");
          Cookies.set("st", result.payload.data.token);
          updateProfile(result.payload.data);
          router.push(NAVIGATION.DASHBOARD_HOME);
        } else {
          handleError(
            result?.payload?.response?.status,
            result?.payload?.response?.data?.msg?.id
          );
        }
      });
    },
    [dispatch, handleError, handleSuccess, router, updateProfile]
  );

  const signUp = useCallback(
    (payload: IRegisterInput) => {
      dispatch(REQUEST_REGISTER(payload)).then((result) => {
        if (result.meta.requestStatus === "fulfilled") {
          handleSuccess("Akun anda berhasil dibuat");
          Cookies.set("st", result.payload.data.token);
          updateProfile(result.payload.data);
          router.push(NAVIGATION.DASHBOARD_HOME);
        } else {
          handleError(
            result?.payload?.response?.status,
            result?.payload?.response?.data?.msg?.id
          );
        }
      });
    },
    [dispatch, handleError, handleSuccess, router, updateProfile]
  );

  const loginGoogle = useCallback(
    async (token: string) => {
      dispatch(REQUEST_SIGN_IN_GOOGLE(token)).then((result) => {
        if (result.meta.requestStatus === "fulfilled") {
          handleSuccess("berhasil login");
          Cookies.set("st", result.payload.data.token);
          updateProfile(result.payload.data);
          router.push(NAVIGATION.DASHBOARD_HOME);
        } else {
          handleError(
            result?.payload?.response?.status,
            result?.payload?.response?.data?.msg?.id
          );
        }
      });
    },
    [dispatch, handleError, handleSuccess, router, updateProfile]
  );

  const getGoogleLoginUrl = useCallback((unique: string) => {
    const googleAuthEndpoint = process.env.NEXT_PUBLIC_GOOGLE_AUTH_URL;
    const loginRequestParameters: { [key: string]: string } = {
      response_type: "code",
      redirect_uri: process.env.NEXT_PUBLIC_GOOGLE_REDIRECT_URL as string,
      scope: "https://www.googleapis.com/auth/userinfo.email",
      client_id: process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID as string,
      state: unique,
    };
    const paramsString = Object.keys(loginRequestParameters)
      .map((key) => `${key}=${encodeURIComponent(loginRequestParameters[key])}`)
      .join("&");

    return `${googleAuthEndpoint}?${paramsString}`;
  }, []);

  const googleAuth = useCallback(async () => {
    const unique = crypto.randomBytes(20).toString("hex");
    const url = getGoogleLoginUrl(unique);
    location.replace(url);
  }, [getGoogleLoginUrl]);

  const logout = useCallback(async () => {
    const resp = await axios.post("/api/logout");
    handleSuccess(resp.data.message);
    router.replace("/");
  }, [handleSuccess, router]);

  return {
    data,
    pending,
    error,
    login,
    signUp,
    googleAuth,
    loginGoogle,
    logout,
  };
};

export default useAuthentication;
