import {
  GlobalContext,
  IGlobalContext,
} from "@/simtest-context/global.context";
import CartList from "@/simtest-page-modules/dashboard/screens/carts/components/cart-list";
import {
  CART_SELECTOR_COLLECTION,
  REQUEST_ADD_CART,
  REQUEST_CART_LIST,
  REQUEST_DELETE_CART,
  REQUEST_RESET_CART,
  REQUEST_RESET_VOUCHER,
} from "@/simtest-redux/carts";
import { useAppDispatch, useAppSelector } from "@/simtest-redux/useRedux";
import { useCallback, useContext, useState } from "react";
import useResponse from "../useResponses";
import { ICartResponse } from "@/simtest-type/carts/carts.interface";
import Cookies from "js-cookie";

const useCart = () => {
  const { modal, drawer } = useContext(GlobalContext) as IGlobalContext;
  const dispatch = useAppDispatch();
  const { handleError, handleSuccess } = useResponse();
  const {
    cartList,
    pending,
    success,
    error,
    refetch,
    voucher,
    totalChartItem,
  } = useAppSelector(CART_SELECTOR_COLLECTION);
  const [totalAmount, setTotalAmount] = useState<number>(0);

  const getCartList = useCallback(
    (voucher: string) => {
      setTotalAmount(0);
      dispatch(REQUEST_CART_LIST(voucher)).then((result) => {
        Cookies.set("cht_cnt", result?.payload?.data?.length);
        if (
          result.meta.requestStatus === "fulfilled" &&
          result.payload.data?.length
        ) {
          const totalPrice = result.payload.data.reduce(
            (total: number, currentPackage: ICartResponse) => {
              return total + currentPackage.package_question.price;
            },
            0
          );
          setTotalAmount(totalPrice);
        }
      });
    },
    [dispatch]
  );

  const addToCart = useCallback(
    (package_question_id: string) => {
      modal?.onClose();
      dispatch(REQUEST_ADD_CART(package_question_id)).then((result) => {
        if (result.meta.requestStatus === "fulfilled") {
          handleSuccess("Berhasil Ditambahkan ke Keranjang");
          drawer?.onOpen();
          drawer?.setContent({
            header: `Item Keranjang anda (${cartList?.length || 0})`,
            body: <CartList type="drawer" />,
            placement: "left",
          });
        } else {
          handleError(
            result?.payload?.response?.status,
            result?.payload?.response?.data?.msg?.id
          );
        }
      });
    },
    [cartList, dispatch, drawer, handleError, handleSuccess, modal]
  );

  const deleteCart = useCallback(
    (id: number) => {
      dispatch(REQUEST_DELETE_CART(id)).then((result) => {
        if (result.meta.requestStatus === "fulfilled") {
          handleSuccess("Keranjang anda berhasil di update");
        } else {
          handleError(
            result?.payload?.response?.status,
            result?.payload?.response?.data?.msg?.id
          );
        }
      });
    },
    [dispatch, handleError, handleSuccess]
  );

  const resetVoucher = useCallback(() => {
    dispatch(REQUEST_RESET_VOUCHER());
  }, [dispatch]);

  const resetCart = useCallback(() => {
    dispatch(REQUEST_RESET_CART());
  }, [dispatch]);

  return {
    resetCart,
    resetVoucher,
    deleteCart,
    addToCart,
    getCartList,
    totalAmount,
    cartList,
    pending,
    success,
    error,
    refetch,
    voucher,
    totalChartItem,
  };
};

export default useCart;
