export const renderTransactionStatus = (status: string) => {
  let text;
  let color;
  switch (status) {
    case "created":
      text = "Belum Bayar";
      color = "#2F6FED";
      break;
    case "unpaid":
      text = "Belum Bayar";
      color = "#2F6FED";
      break;
    case "expired":
      text = "Batal";
      color = "#E02D3C";
      break;
    case "paid":
      text = "Selesai";
      color = "#08875D";
      break;
    default:
      text = "Selesai";
      color = "#08875D";
      break;
  }
  return { text, color };
};
