import { ETestType } from "@/simtest-type/package/package.interface";

export const renderModuleType = (type: string) => {
  let color;
  let logo;
  let name;

  switch (type) {
    case ETestType.BUMN:
      color = "#142141";
      logo = "../assets/logo/logo-bumn.svg";
      name = "BUMN";
      break;
    case ETestType.LPDP:
      color = "#EE7121";
      logo = "../assets/logo/logo-lpdp.svg";
      name = "LPDP";
      break;
    case ETestType.CPNS:
      color = "#CC9A36";
      logo = "../assets/logo/logo-cpns.svg";
      name = "CPNS";
      break;
    default:
      color = "#142141";
      logo = "../assets/logo/logo-bumn.svg";
      name = "BUMN";
      break;
  }
  return { color, logo, name };
};
