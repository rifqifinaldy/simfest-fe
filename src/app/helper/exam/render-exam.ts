import { EAnswerType } from "@/simtest-type/exam/exam-answer.interface";

export const examAnswerColor = (value: string) => {
  switch (value) {
    case EAnswerType.ANSWERED:
      return "success.700";
    case EAnswerType.HESISTANT:
      return "secondary.700";
    case EAnswerType.ON_GOING:
      return "primary.700";
    default:
      return "#B6BEC2";
  }
};
