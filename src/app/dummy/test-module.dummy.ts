export const testModuleDummy = [
  {
    id: 0,
    title: "LPDP",
  },
  {
    id: 1,
    title: "BUMN",
  },
  {
    id: 2,
    title: "GRE®",
  },
  {
    id: 3,
    title: "GMAT®",
  },
  {
    id: 4,
    title: "Admissions",
  },
  {
    id: 5,
    title: "TOEFL®",
  },
  {
    id: 6,
    title: "ACT®",
  },
  {
    id: 7,
    title: "LSAT®",
  },
  {
    id: 8,
    title: "MCAT®",
  },
  {
    id: 9,
    title: "IELTS®",
  },
];
