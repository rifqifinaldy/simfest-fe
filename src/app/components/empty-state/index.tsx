import { Button, Flex, Text } from "@chakra-ui/react";
import { AiOutlineWarning } from "react-icons/ai";
import React from "react";

interface EmptyStateProps {
  title: string;
  showButton?: boolean;
  buttonText?: string;
  buttonAction?: () => void;
}

const EmptyState: React.FC<EmptyStateProps> = ({
  title,
  showButton,
  buttonText,
  buttonAction,
}) => {
  return (
    <Flex
      w="full"
      h="full"
      justifyContent="center"
      alignItems="center"
      flexDir="column"
      gap="20px"
    >
      <AiOutlineWarning size="200px" color="#FFC007" />
      <Text variant="heading-h5">{title}</Text>
      {showButton && (
        <Button
          colorScheme="blue"
          minW="150px"
          onClick={() => {
            if (buttonAction) {
              buttonAction();
            }
          }}
        >
          {buttonText}
        </Button>
      )}
    </Flex>
  );
};

export default EmptyState;
