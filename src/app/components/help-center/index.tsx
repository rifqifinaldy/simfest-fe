import {
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
} from "@chakra-ui/react";
import React from "react";
import { AiFillInstagram } from "react-icons/ai";
import { BiLogoTelegram, BiSolidChat } from "react-icons/bi";

const HelpCenter = () => {
  return (
    <Menu>
      <MenuButton
        size="32px"
        as={IconButton}
        aria-label="Options"
        icon={<BiSolidChat />}
        variant={{ base: "ghost", lg: "outline" }}
      />
      <MenuList minW="50px" maxW="50px" ml="70px" cursor="pointer">
        <MenuItem
          fontSize="24px"
          as="a"
          href="https://www.instagram.com/simulasitestdotcom/"
          target="_blank"
        >
          <AiFillInstagram />
        </MenuItem>
        <MenuItem
          fontSize="24px"
          as="a"
          href="https://t.me/simulasitest"
          target="_blank"
        >
          <BiLogoTelegram />
        </MenuItem>
      </MenuList>
    </Menu>
  );
};

export default HelpCenter;
