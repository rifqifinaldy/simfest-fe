import { Flex, Spinner, Text } from "@chakra-ui/react";
import React from "react";

interface PageLoaderProps {
  text: string;
}

const PageLoader: React.FC<PageLoaderProps> = ({ text }) => {
  return (
    <Flex
      flexDir="column"
      justifyContent="center"
      alignItems="center"
      w="full"
      h="100vh"
      background="gray.400"
      gap="20px"
    >
      <Spinner thickness="12px" speed="0.85s" w={24} h={24} boxSize={24} />
      <Text color="blackAlpha.700" fontWeight="semibold" fontSize="21px">
        {text} ...
      </Text>
    </Flex>
  );
};

export default PageLoader;
