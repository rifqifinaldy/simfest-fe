import {
  Box,
  Button,
  Flex,
  Image,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Text,
} from "@chakra-ui/react";
import React, { useContext } from "react";
import { sidebarItem } from "../sidebar.config";
import { useRouter } from "next/router";
import { FaChevronDown, FaSignOutAlt, FaUser } from "react-icons/fa";
import {
  GlobalContext,
  IGlobalContext,
} from "@/simtest-context/global.context";
import useAuthentication from "@/simtest-hook/useAuthentication";
import { NAVIGATION } from "@/simtest-type/navigation.interface";

const DekstopNavigation: React.FC = () => {
  const router = useRouter();
  const { user } = useContext(GlobalContext) as IGlobalContext;

  const { logout } = useAuthentication();

  return (
    <Flex
      display={{ base: "none", lg: "block" }}
      position="relative"
      flexDir="column"
      justifyContent="space"
      w="256px"
      pl="20px"
      pt="40px"
    >
      <Box ml="20px" mb="40px" onClick={() => router.push(NAVIGATION.HOMEPAGE)}>
        <Image
          alt="simulasitest"
          src="/assets/logo/logo-simtest-1.png"
          w="100px"
        />
      </Box>
      {sidebarItem.map((item) => {
        return (
          <Flex
            cursor="pointer"
            key={item.id}
            onClick={() => router.push(item.to)}
            gap="10px"
            my="10px"
            alignItems="center"
            opacity={router.asPath === item.to ? "1" : "0.65"}
            color={router.asPath === item.to ? "#1E4EAE" : "#1D2433"}
            borderRight={
              router.asPath === item.to ? "4px solid #1E4EAE" : "none"
            }
            fontSize="16px"
            fontWeight={router.asPath === item.to ? "700" : "600"}
            lineHeight="22.4px"
            letterSpacing="0.2px"
          >
            <Box>{item.icon}</Box>
            <Text>{item.title}</Text>
          </Flex>
        );
      })}
      <Box mr="20px" position="absolute" bottom="20" w="80%">
        <Menu>
          <MenuButton
            rightIcon={<FaChevronDown />}
            size="sm"
            w="full"
            as={Button}
            leftIcon={<FaUser />}
          >
            <Text
              maxW="100%"
              textOverflow="ellipsis"
              whiteSpace="nowrap"
              overflow="hidden"
            >
              {user?.profile?.name}
            </Text>
          </MenuButton>
          <MenuList>
            <MenuItem
              onClick={() => logout()}
              display="flex"
              gap="10px"
              alignItems="center"
            >
              <FaSignOutAlt /> Logout
            </MenuItem>
          </MenuList>
        </Menu>
      </Box>
    </Flex>
  );
};

export default DekstopNavigation;
