import {
  GlobalContext,
  IGlobalContext,
} from "@/simtest-context/global.context";
import useAuthentication from "@/simtest-hook/useAuthentication";
import { NAVIGATION } from "@/simtest-type/navigation.interface";
import {
  Box,
  Flex,
  IconButton,
  Image,
  Menu,
  MenuButton,
  MenuGroup,
  MenuItem,
  MenuList,
  Text,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import React, { useContext } from "react";
import { AiFillInstagram, AiOutlineMenu } from "react-icons/ai";
import { BiLogoTelegram } from "react-icons/bi";
import { FaSignOutAlt } from "react-icons/fa";

const MobileTopbar: React.FC = () => {
  const router = useRouter();
  const { user } = useContext(GlobalContext) as IGlobalContext;
  const { logout } = useAuthentication();

  return (
    <Flex
      display={{ base: "flex", lg: "none" }}
      bg="primary.700"
      h={{ base: "60px", lg: "80px" }}
      px={{ base: "20px", lg: "80px" }}
      w="full"
      top="0"
      alignItems="center"
      position="fixed"
      justifyContent="space-between"
      borderBottom="1px solid blue"
      zIndex={999}
    >
      <Flex>
        <Image
          onClick={() => router.push(NAVIGATION.HOMEPAGE)}
          alt="simulasitest"
          src="../assets/logo/logo-simtest.png"
          w="100px"
        />
        {user?.profile?.name && (
          <Box ml="10px">
            <Text opacity="0.75">Halo</Text>
            <Text>{user?.profile?.name}</Text>
          </Box>
        )}
      </Flex>
      <Menu>
        <MenuButton
          as={IconButton}
          aria-label="Options"
          icon={<AiOutlineMenu />}
          variant="ghosted"
        />
        <MenuList>
          <MenuGroup title="Help">
            <MenuItem
              icon={<AiFillInstagram size="18px" />}
              as="a"
              href="https://www.instagram.com/simulasitestdotcom/"
              target="_blank"
            >
              Instagram
            </MenuItem>
            <MenuItem
              icon={<BiLogoTelegram size="18px" />}
              as="a"
              href="https://t.me/simulasitest"
              target="_blank"
            >
              {" "}
              Telegram
            </MenuItem>
          </MenuGroup>
          <MenuGroup title="Account">
            <MenuItem
              onClick={() => logout()}
              icon={<FaSignOutAlt size="14px" />}
            >
              Logout
            </MenuItem>
          </MenuGroup>
        </MenuList>
      </Menu>
    </Flex>
  );
};

export default MobileTopbar;
