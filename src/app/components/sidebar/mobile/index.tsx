import { Flex, Text } from "@chakra-ui/react";
import React from "react";
import { sidebarItem } from "../sidebar.config";
import { useRouter } from "next/router";

const MobileNavigation: React.FC = () => {
  const router = useRouter();

  return (
    <Flex
      display={{ base: "flex", lg: "none" }}
      position="fixed"
      bottom="0"
      w="full"
      bg="neutral.0"
      shadow="sm"
      gap={{ base: "40px", sm: "80px" }}
      p="20px"
      justifyContent="center"
      borderTop="2px solid #E1E6EF"
    >
      {sidebarItem.map((item) => {
        return (
          <Flex
            onClick={() => router.push(item.to)}
            flexDir="column"
            key={item.id}
            alignItems="center"
            opacity={router.asPath === item.to ? "1" : "0.65"}
            color={router.asPath === item.to ? "#1E4EAE" : "#1D2433"}
            textAlign="center"
            gap="5px"
          >
            <Text fontSize={{ base: "18px", sm: "22px" }}>{item.icon}</Text>
            <Text
              lineHeight={{ base: "14px", sm: "18px" }}
              fontSize={{ base: "14px", sm: "18px" }}
              variant="heading-h7"
            >
              {item.mobileTitle}
            </Text>
          </Flex>
        );
      })}
    </Flex>
  );
};

export default MobileNavigation;
