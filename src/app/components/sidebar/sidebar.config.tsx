import { NAVIGATION } from "@/simtest-type/navigation.interface";
import {
  AiFillAccountBook,
  AiFillCreditCard,
  AiFillFileText,
  // AiFillPieChart,
} from "react-icons/ai";
import { FaShoppingCart } from "react-icons/fa";

export const sidebarItem = [
  // {
  //   id: 0,
  //   title: "My Performance",
  //   to: NAVIGATION.DASHBOARD_HOME,
  //   icon: <AiFillPieChart />,
  // },
  {
    id: 1,
    title: "Ujian Saya",
    mobileTitle: "Ujian",
    to: NAVIGATION.DASHBOARD_MY_EXAM,
    icon: <AiFillFileText />,
  },
  {
    id: 2,
    title: "List Produk",
    mobileTitle: "Produk",
    to: NAVIGATION.DASHBOARD_PRODUCT,
    icon: <AiFillAccountBook />,
  },
  {
    id: 3,
    title: "Daftar Transaksi",
    mobileTitle: "Transaksi",
    to: NAVIGATION.DASHBOARD_TRANSACTION,
    icon: <AiFillCreditCard />,
  },
  {
    id: 4,
    title: "Keranjang",
    mobileTitle: "Keranjang",
    to: NAVIGATION.DASHBOARD_CART,
    icon: <FaShoppingCart />,
  },
];
