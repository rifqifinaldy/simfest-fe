import { Box, Button, Flex, Image, Text } from "@chakra-ui/react";
import React, { useContext } from "react";
import { navbarItem } from "./navbar.config";
import { useRouter } from "next/router";
import {
  GlobalContext,
  IGlobalContext,
} from "@/simtest-context/global.context";
import useCart from "@/simtest-hook/useCart";
import Link from "next/link";

const Navbar: React.FC = () => {
  const { user } = useContext(GlobalContext) as IGlobalContext;
  const router = useRouter();

  const { totalChartItem } = useCart();

  return (
    <>
      <Flex
        bg="primary.700"
        h={{ base: "60px", lg: "80px" }}
        px={{ base: "20px", lg: "80px" }}
        w="full"
        top="0"
        alignItems="center"
        justifyContent="space-between"
        borderBottom="1px solid blue"
      >
        <Flex>
          <Image
            alt="simulasitest"
            src="./assets/logo/logo-simtest.png"
            w="100px"
          />
          {user?.profile?.name && (
            <Box ml="10px">
              <Text color="white" opacity="0.75">
                Halo
              </Text>
              <Text color="white">{user?.profile?.name}</Text>
            </Box>
          )}
        </Flex>
        <Flex alignItems="center">
          <Flex gap="25px" display={{ base: "none", lg: "flex" }}>
            {navbarItem.map((item) => {
              return (
                <Box
                  cursor="pointer"
                  key={item.id}
                  scroll={false}
                  as={Link}
                  href={
                    router?.pathname === "/"
                      ? item.to
                      : `/${item.to.replace("/", "")}`
                  }
                >
                  <Text
                    cursor="pointer"
                    variant="body-large-medium"
                    color="white"
                  >
                    {item.title}
                  </Text>
                </Box>
              );
            })}
            <Box
              fontWeight="400"
              color="text.primary-white"
              mr="1.5em"
              ml="3em"
            >
              Cart({totalChartItem})
            </Box>
          </Flex>
          <Button
            size={{ base: "sm", lg: "md" }}
            color="white"
            colorScheme="yellow"
            onClick={() =>
              router.push(user?.profile?.token ? "/dashboard" : "/auth/login")
            }
          >
            {user?.profile?.name ? "Dashboard" : "Masuk"}
          </Button>
        </Flex>
      </Flex>
      {/* Responsiveness in Mobile */}
      <Flex
        display={{ base: "flex", lg: "none" }}
        alignItems="center"
        justifyContent="space-between"
        h="40px"
        bg="primary.700"
        px="20px"
        w="full"
        shadow="lg"
        gap="25px"
      >
        <Flex justifyContent="space-between" alignItems="center" gap="20px">
          {navbarItem.map((item) => {
            return (
              <Box cursor="pointer" key={item.id} as="a" href={item.to}>
                <Text
                  color="white"
                  variant={{
                    base: "body-xsmall-medium",
                  }}
                >
                  {item.title}
                </Text>
              </Box>
            );
          })}
        </Flex>
        <Box fontSize="12px" fontWeight="400" color="text.primary-white">
          Cart({totalChartItem})
        </Box>
      </Flex>
    </>
  );
};

export default Navbar;
