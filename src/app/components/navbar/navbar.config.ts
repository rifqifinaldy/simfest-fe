export const navbarItem = [
  {
    id: 0,
    title: "Home",
    to: "/",
  },
  {
    id: 1,
    title: "Blog",
    to: "/blogs",
  },
  {
    id: 2,
    title: "Pilihan Ujian",
    to: "#package-question",
  },
  {
    id: 3,
    title: "Tentang Kami",
    to: "#about-us",
  },
  {
    id: 4,
    title: "FAQ",
    to: "#faq",
  },

];
