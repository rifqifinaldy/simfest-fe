import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  ResponsiveValue,
} from "@chakra-ui/react";
import React, { Dispatch, SetStateAction } from "react";

export interface SimfestModalContentProps {
  header?: JSX.Element | React.ReactNode | string;
  body?: JSX.Element | React.ReactNode | string;
  footer?: JSX.Element | React.ReactNode | string;
  overlayClose?: boolean;
  closeButton?: boolean;
  size?:
    | ResponsiveValue<
        | (string & {})
        | "sm"
        | "md"
        | "lg"
        | "xl"
        | "2xl"
        | "3xl"
        | "full"
        | "xs"
        | "4xl"
        | "5xl"
        | "6xl"
      >
    | undefined;
  color?: string;
}

export interface SimfestModalProps {
  isOpen: boolean;
  onClose: () => void;
  onOpen: () => void;
  setContent: Dispatch<SetStateAction<SimfestModalContentProps>>;
  content: SimfestModalContentProps;
}

const SimfestModal: React.FC<SimfestModalProps> = ({
  isOpen,
  onClose,
  content,
}) => {
  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      closeOnOverlayClick={content.overlayClose}
      isCentered
      motionPreset="slideInBottom"
      size={content.size || "md"}
      scrollBehavior="inside"
      closeOnEsc={false}
    >
      <ModalOverlay />
      <ModalContent background={content.color}>
        <ModalHeader>{content.header}</ModalHeader>
        {content.closeButton && <ModalCloseButton />}
        <ModalBody display="flex" flexDir="column" gap="16px">
          {content.body}
        </ModalBody>
        <ModalFooter>{content.footer}</ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default SimfestModal;
