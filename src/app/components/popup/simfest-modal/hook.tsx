import { useState } from "react";
import { useDisclosure } from "@chakra-ui/react";
import { SimfestModalContentProps } from ".";

const useModal = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  //   Default Content Modal State
  const [content, setContent] = useState<SimfestModalContentProps>({
    header: "",
    body: "",
    footer: "",
    overlayClose: false,
    closeButton: true,
    size: "md",
  });

  return {
    isOpen,
    onOpen,
    onClose,
    content,
    setContent,
  };
};

export default useModal;
