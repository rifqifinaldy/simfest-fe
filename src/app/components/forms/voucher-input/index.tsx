import {
  Button,
  FormControl,
  FormErrorMessage,
  Input,
  InputGroup,
  InputRightElement,
  Text,
} from "@chakra-ui/react";
import React from "react";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import { AiOutlineArrowRight } from "react-icons/ai";

interface VoucherInputProps {
  submitVoucher: SubmitHandler<FieldValues>;
  isError?: boolean;
  resetVoucher: () => void;
}

const VoucherInput: React.FC<VoucherInputProps> = ({
  submitVoucher,
  isError,
  resetVoucher,
}) => {
  const methods = useForm({});

  const { handleSubmit, register } = methods;

  return (
    <form onSubmit={handleSubmit(submitVoucher)}>
      <FormControl isInvalid={Boolean(isError)}>
        <Text fontWeight="bold" fontSize="16px" color="#000000" mb="20px">
          Masukan Kode Kupon
        </Text>
        <InputGroup>
          <Input
            id="voucher"
            type="text"
            size="lg"
            bg="#F8F9FC"
            shadow="sm"
            placeholder="Masukan Kode Kupon"
            {...register("voucher")}
            onChange={() => resetVoucher()}
          />
          <InputRightElement h="full" w="50px">
            <Button
              my="auto"
              colorScheme="blue"
              h="full"
              type="submit"
              w="100px"
            >
              <Text fontSize="14px">
                <AiOutlineArrowRight />
              </Text>
            </Button>
          </InputRightElement>
        </InputGroup>
        <FormErrorMessage>Kode Kupon Anda Tidak Valid</FormErrorMessage>
      </FormControl>
    </form>
  );
};

export default VoucherInput;
