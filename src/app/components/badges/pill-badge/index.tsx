import { Flex, Text } from "@chakra-ui/react";
import React from "react";

interface PillBadgeProps {
  content: React.ReactNode;
}

const PillBadge: React.FC<PillBadgeProps> = ({ content }) => {
  return (
    <Flex
      background="primary.800"
      justifyContent="center"
      alignItems="center"
      py="0.75em"
      color="white"
      border="1px solid white"
      borderRadius="40px"
      w="auto"
    >
      <Text as="h4" variant="body-small-bold" color="white">
        {content}
      </Text>
    </Flex>
  );
};

export default PillBadge;
