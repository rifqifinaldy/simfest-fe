import { Box, Button, Flex, Image, Text } from "@chakra-ui/react";
import React from "react";
import { TTestType } from "type/package/package.interface";
import { AiOutlineClockCircle, AiOutlineFileText } from "react-icons/ai";
import { renderModuleType } from "helper/package-question/render-module-type";
interface SampleTestCardProps {
  moduleType: TTestType;
  status?: boolean | null;
  name: string;
  quantity: number;
  duration: number;
  action?: () => void;
}

const SampleTestCard: React.FC<SampleTestCardProps> = ({
  moduleType,
  status,
  name,
  quantity,
  duration,
  action,
}) => {
  return (
    <Box borderRadius="10px" shadow="lg" pb="1em" overflow="hidden">
      <Flex
        py="0.5em"
        px="0.75em"
        background={renderModuleType(moduleType).color}
        justifyContent="space-between"
        alignItems="center"
      >
        <Flex alignItems="center" gap="5px">
          <Image src={renderModuleType(moduleType).logo} alt={moduleType} />
          <Text as="h2" color="neutral.0" variant="body-medium-bold" ml="0.5em">
            {moduleType}
          </Text>
        </Flex>
        {status && (
          <Flex
            background="success.700"
            flexDir="column"
            justifyContent="center"
            alignItems="center"
            px="0.5em"
            py="0.25em"
            borderRadius="8px"
            color="neutral.0"
            textTransform="capitalize"
          >
            <Text variant="body-medium-bold" color="white">
              Dibeli
            </Text>
          </Flex>
        )}
      </Flex>
      <Flex flexDir="column" px="12px" pt="1em">
        <Text h="36px" as="h3" variant="heading-h6" mb="1em">
          {name}
        </Text>
        <Flex gap="10px">
          <AiOutlineFileText />
          <Text variant="body-medium-medium" mb="0.5em">
            Pertanyaan : {quantity} item
          </Text>
        </Flex>
        <Flex gap="10px" alignItems="center">
          <AiOutlineClockCircle />
          <Text variant="body-medium-medium">Durasi: {duration} Menit</Text>
        </Flex>
        <Button size="md" mt="2em" onClick={action} colorScheme="blue">
          {status ? "Coba Ujian" : "Simulasi Test"}
        </Button>
      </Flex>
    </Box>
  );
};

export default SampleTestCard;
