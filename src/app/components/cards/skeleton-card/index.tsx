import { Skeleton } from "@chakra-ui/react";
import React from "react";

interface SkeletonCardProps {
  count: 4;
}

const SkeletonCard: React.FC<SkeletonCardProps> = ({ count }) => {
  return (
    <>
      {[...Array(count)].map((_, index) => {
        return (
          <Skeleton height="200px" key={index}>
            Skeleton
          </Skeleton>
        );
      })}
    </>
  );
};

export default SkeletonCard;
