import { BORDER, RADIUS } from "@/simtest-styles/style.constant";
import { Flex, Image, Text } from "@chakra-ui/react";
import dayjs from "dayjs";
import React from "react";

interface BlogCardProps {
  title: string;
  thumbnailCaption: string;
  thumbnailUrl: string;
  postedAt: string;
  highlightText: string;
}

const BlogsCard: React.FC<BlogCardProps> = ({
  title,
  thumbnailCaption,
  thumbnailUrl,
  postedAt,
  highlightText,
}) => {
  return (
    <Flex
      shadow="sm"
      flexDir="column"
      border={BORDER.DEFAULT}
      rounded={RADIUS.SM}
      overflow="hidden"
      cursor="pointer"
      maxH={{ md: "400px" }}
      minH="400px"
      maxW={{ base: "full", "2xl": "400px" }}
    >
      <Image
        borderBottom={BORDER.DEFAULT}
        src={thumbnailUrl}
        alt={thumbnailCaption}
        w="full"
        minH="200px"
        maxH={{ md: "200px" }}
        shadow="sm"
      />
      <Flex flexDir="column" p="20px">
        <Text
          maxH={{ md: "100px" }}
          mb="10px"
          role="h2"
          variant="heading-h4"
          color="neutral.700"
          maxW={{ md: "380px" }}
          whiteSpace={{ base: "normal", md: "nowrap" }}
          overflow="hidden"
          textOverflow="ellipsis"
        >
          {title}
        </Text>
        <Text mt="12px" mb="8px" variant="body-small-bold" color="gray.500">
          {dayjs(postedAt).format("DD MMMM YYYY")}
        </Text>
        <Text textAlign="justify" variant="body-medium-medium">
          {highlightText}
        </Text>
      </Flex>
    </Flex>
  );
};

export default BlogsCard;
