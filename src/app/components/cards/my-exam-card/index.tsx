import { NAVIGATION } from "@/simtest-type/navigation.interface";
import { Badge, Button, Flex, Text } from "@chakra-ui/react";
import { useRouter } from "next/router";
import React, { useCallback } from "react";
import { AiOutlineClockCircle, AiOutlineFileText } from "react-icons/ai";

interface MyExamCardProps {
  id: number | string;
  package_name: string;
  title: string;
  quantity: number;
  duration: number;
  correctAnswer: number;
  startExam?: () => void;
  isAnswered: boolean;
  exam_id: string;
}

const MyExamCard: React.FC<MyExamCardProps> = ({
  id,
  package_name,
  title,
  quantity,
  duration,
  correctAnswer,
  isAnswered,
  exam_id,
}) => {
  const router = useRouter();

  const buttonText = useCallback(
    (id: string) => {
      if (localStorage.getItem(id)) {
        return "Lanjutkan";
      } else if (isAnswered) {
        return "Coba Lagi";
      } else {
        return "Mulai Ujian";
      }
    },
    [isAnswered]
  );

  return (
    <Flex
      flexDir="column"
      background="primary.100"
      my="10px"
      py="10px"
      px="20px"
      justifyContent="space-between"
      borderRadius="10px"
    >
      <Flex mb="10px" justifyContent="space-between" alignItems="center">
        <Text variant="heading-h6">{title}</Text>
        {isAnswered ? (
          <Badge colorScheme="green" textTransform="capitalize">
            Telah Dicoba
          </Badge>
        ) : (
          <Badge colorScheme="warning" textTransform="capitalize">
            Perlu Diselesaikan
          </Badge>
        )}
      </Flex>
      <Flex flexDir="row" gap="15px" pb="15px" borderBottom="1px solid #E2E8F0">
        <Flex gap="5px" alignItems="center">
          <AiOutlineFileText />
          <Text variant="body-medium-medium">{quantity} Soal</Text>
        </Flex>
        <Flex gap="5px" alignItems="center">
          <AiOutlineClockCircle />
          <Text variant="body-medium-medium">{duration} Menit</Text>
        </Flex>
      </Flex>
      <Flex
        flexDir="column"
        alignItems="flex-start"
        py="15px"
        justifyContent="center"
      >
        <Text
          opacity="0.6"
          fontSize="14px"
          fontWeight="400"
          color="text.primary-black"
        >
          Correct Answers
        </Text>
        <Text variant="heading-h4">{`${correctAnswer} / ${quantity}`}</Text>
      </Flex>
      <Flex flexDir="column" gap="10px">
        <Button
          onClick={() =>
            router.push({
              pathname: `${NAVIGATION.DISCUSSION}/${exam_id}`,
              query: { n: title, p: package_name },
            })
          }
          colorScheme="blue"
          variant="outline"
          isDisabled={!Boolean(exam_id)}
        >
          Pembahasan
        </Button>
        <Button
          onClick={() =>
            router.push({
              pathname: `${NAVIGATION.EXAM}/${id}`,
              query: { t: duration, n: title, p: package_name },
            })
          }
          colorScheme="blue"
        >
          {buttonText(id as string)}
        </Button>
      </Flex>
    </Flex>
  );
};

export default MyExamCard;
