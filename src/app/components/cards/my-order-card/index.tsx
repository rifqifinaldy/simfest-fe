import { NAVIGATION } from "@/simtest-type/navigation.interface";
import { IOrderDetail } from "@/simtest-type/order/order.interface";
import {
  Box,
  Button,
  Flex,
  ListItem,
  Text,
  UnorderedList,
} from "@chakra-ui/react";
import dayjs from "dayjs";
import { rupiah } from "helper/idr-converter";
import { renderTransactionStatus } from "helper/transaction/transaction.helper";
import { useRouter } from "next/router";
import React, { useCallback } from "react";

interface MyOrderCardProps {
  id: string;
  status: string;
  paidAt: string;
  productList: IOrderDetail[];
  totalAmount: number;
  paymentToken: string;
}

const MyOrderCard: React.FC<MyOrderCardProps> = ({
  id,
  status,
  paidAt,
  productList,
  totalAmount,
  paymentToken,
}) => {
  const router = useRouter();
  const redirectToXendit = useCallback(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (window as any).snap.pay(paymentToken, {
      onSuccess: () => {
        router.push(`${NAVIGATION.DASHBOARD_MY_EXAM}`);
      },
      onPending: () => {
        router.push(`${NAVIGATION.DASHBOARD_TRANSACTION}`);
      },
      onError: () => {
        alert("payment failed!");
        router.push(`${NAVIGATION.DASHBOARD_TRANSACTION}`);
      },
      onClose: () => {
        router.push(`${NAVIGATION.DASHBOARD_TRANSACTION}`);
      },
    });
  }, [paymentToken, router]);

  return (
    <Flex
      p="20px"
      flexDir="column"
      bg="#fff"
      rounded="10px"
      shadow="sm"
      minH="320px"
      justifyContent="space-between"
    >
      <Text variant="heading-h6"> Order #{id}</Text>
      <Box py="10px" borderBottom="1px solid #EBF0FF">
        <Flex justifyContent="space-between" alignItems="center" my="5px">
          <Text variant="body-small-regular"> Status Transaksi </Text>
          <Text
            variant="body-small-bold"
            color={renderTransactionStatus(status).color}
          >
            {renderTransactionStatus(status).text}
          </Text>
        </Flex>
        {paidAt && status === "created" && (
          <Flex justifyContent="space-between" alignItems="center" my="5px">
            <Text variant="body-small-regular">Bayar Sebelum: </Text>
            <Text variant="body-small-bold">
              {dayjs(paidAt).format("DD MMMM YYYY, HH:MM")} WIB
            </Text>
          </Flex>
        )}
      </Box>
      <Box py="10px">
        <Text variant="body-medium-semibold">Product Dibeli :</Text>
        <UnorderedList px="10px">
          {productList?.map((list) => {
            return (
              <ListItem key={list.Id}>
                <Text variant="body-small-regular">{list?.package?.name}</Text>
              </ListItem>
            );
          })}
        </UnorderedList>
      </Box>
      <Box py="10px">
        <Text variant="body-medium-semibold">Total:</Text>
        <Text variant="heading-h4">{rupiah.format(totalAmount)}</Text>
      </Box>
      <Box pt="10px" w="full">
        {status === "created" && (
          <Button
            onClick={() => redirectToXendit()}
            w="full"
            colorScheme="blue"
          >
            Bayar
          </Button>
        )}
      </Box>
    </Flex>
  );
};

export default MyOrderCard;
