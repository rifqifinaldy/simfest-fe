import React from "react";
import { Flex, Box, Text } from "@chakra-ui/react";
import { footerNavitaionItem } from "./footer-navigation.config";

const FooterNavigation: React.FC = () => {
  return (
    <Flex
      flexDirection={{ base: "column", lg: "row" }}
      px={{ base: "20px", lg: "80px" }}
      py={{ base: "20px", lg: "40px" }}
      justifyContent="space-between"
    >
      <Box>
        <Text variant="heading8">© Copyright STCOM 2023</Text>
      </Box>
      <Flex alignItems="center" mt={{ base: "0.5em", lg: "0" }}>
        <Flex gap={{ base: "10px", lg: "25px" }}>
          {footerNavitaionItem.map((item) => {
            return (
              <Box cursor="pointer" key={item.id} as="a" href={item.to}>
                <Text variant="body-large-medium">{item.title}</Text>
              </Box>
            );
          })}
        </Flex>
      </Flex>
    </Flex>
  );
};

export default FooterNavigation;
