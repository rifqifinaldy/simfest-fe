export const footerNavitaionItem = [
  {
    id: 0,
    title: "Home",
    to: "/",
  },
  {
    id: 1,
    title: "Tentang Kami",
    to: "#about-us",
  },
  {
    id: 2,
    title: "Kursus",
    to: "#package-question",
  },
  {
    id: 3,
    title: "FAQ",
    to: "#faq",
  },
];
