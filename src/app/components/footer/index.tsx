import React from "react";
import { Flex, Box, Text } from "@chakra-ui/react";
import FooterContact from "./footer-contact";
import FooterNavigation from "./footer-navigation";

const Footer: React.FC = () => {
  return (
    <>
      <Flex
        as="section"
        position="relative"
        flexDirection={{ base: "column", lg: "row" }}
        justifyContent="space-between"
        bg="primary.700"
        mt={{ base: "40px", lg: "80px" }}
        p={{ base: "20px", lg: "80px" }}
        pb={{ base: "0", lg: "20px" }}
      >
        <Box>
          <Text as="h1" variant="heading-h4" color="white" mb="0.5em">
            SIMULASI TEST
          </Text>
          <Text variant="body-large-regular" color="white">
            Bangun dan wujudkan cita bersama Simulasi Test
          </Text>
        </Box>
        <Flex
          flexDirection={{ base: "column", lg: "row" }}
          gap={{ base: "20px", lg: "80px" }}
          mt={{ base: "2em", md: "0" }}
        >
          <Box>
            <Text
              variant="heading7"
              color="white"
              mb={{ base: "0.5em", lg: "1.5em" }}
            >
              Social Media
            </Text>
            <Flex flexDir="column" gap={{ base: "5px", lg: "10px" }}>
              <Text
                variant="heading8"
                color="#fff"
                as="a"
                href="https://www.instagram.com/simulasitestdotcom/"
                target="_blank"
              >
                Instagram
              </Text>
              <Text variant="heading8" color="white">
                Twitter
              </Text>
              <Text variant="heading8" color="white">
                Linkedin
              </Text>
            </Flex>
          </Box>
        </Flex>
        <FooterContact />
      </Flex>
      <FooterNavigation />
    </>
  );
};

export default Footer;
