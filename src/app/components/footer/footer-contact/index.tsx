import { Box, Flex, Link, Text } from "@chakra-ui/react";
import { SOCIAL } from "helper/social-media";
import React from "react";

const FooterContact: React.FC = () => {
  return (
    <Flex
      position={{ base: "static", lg: "absolute" }}
      bottom="0"
      justifyContent="space-between"
      p="20px"
      background="secondary.700"
      w={{ base: "calc(100% + 40px)", lg: "450px" }}
      ml={{ base: "-20px", lg: "0" }}
      flexDir={{ base: "column", lg: "row" }}
      mt={{ base: "1.5em", lg: "0" }}
    >
      <Box>
        <Text fontSize="20px" fontWeight="600">
          Email
        </Text>
        <Text fontSize="16px"> simulasitestdotcom@gmail.com </Text>
      </Box>
      <Box>
        <Text fontSize="20px" fontWeight="600">
          Telephone
        </Text>
        <Text fontSize="16px" as={Link} href={SOCIAL.TELEGRAM} target="_blank">
          +{SOCIAL.WA_PHONE_NUMBER}
        </Text>
      </Box>
    </Flex>
  );
};

export default FooterContact;
