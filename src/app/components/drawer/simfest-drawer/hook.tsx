import { useState } from "react";
import { useDisclosure } from "@chakra-ui/react";
import { SimfestDrawerContentProps } from ".";

const useDrawer = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  //   Default Content Modal State
  const [content, setContent] = useState<SimfestDrawerContentProps>({
    header: "",
    body: "",
    footer: "",
    placement: "right",
    size: "xl",
  });

  return {
    isOpen,
    onOpen,
    onClose,
    content,
    setContent,
  };
};

export default useDrawer;
