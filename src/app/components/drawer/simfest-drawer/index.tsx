import {
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  ResponsiveValue,
} from "@chakra-ui/react";
import React, { Dispatch, SetStateAction } from "react";

export interface SimfestDrawerContentProps {
  header?: JSX.Element | React.ReactNode | string;
  body?: JSX.Element | React.ReactNode | string;
  footer?: JSX.Element | React.ReactNode | string;
  placement: "right" | "left";
  overlayClose?: boolean;
  size?:
    | ResponsiveValue<
        | (string & {})
        | "sm"
        | "md"
        | "lg"
        | "xl"
        | "2xl"
        | "3xl"
        | "full"
        | "xs"
        | "4xl"
        | "5xl"
        | "6xl"
      >
    | undefined;
}

export interface SimfestDrawerProps {
  isOpen: boolean;
  onClose: () => void;
  onOpen: () => void;
  setContent: Dispatch<SetStateAction<SimfestDrawerContentProps>>;
  content: SimfestDrawerContentProps;
}

const SimfestDrawer: React.FC<SimfestDrawerProps> = ({
  isOpen,
  onClose,
  content,
}) => {
  return (
    <Drawer size="xl" isOpen={isOpen} placement="right" onClose={onClose}>
      <DrawerOverlay />
      <DrawerContent>
        <DrawerCloseButton />
        <DrawerHeader>{content.header}</DrawerHeader>

        <DrawerBody>{content.body}</DrawerBody>

        <DrawerFooter>{content.footer}</DrawerFooter>
      </DrawerContent>
    </Drawer>
  );
};

export default SimfestDrawer;
