import { Action, configureStore, ThunkAction } from "@reduxjs/toolkit";
import { AUTH_REDUCER } from "./authentication";
import { EXAM_REDUCER } from "./exam";
import { DISCUSSION_REDUCER } from "./discussion";
import { CART_REDUCER } from "./carts";
import { ORDER_REDUCER } from "./order";
import { PACKAGE_REDUCER } from "./package-question";

export const store = configureStore({
  reducer: {
    authentication: AUTH_REDUCER,
    products: PACKAGE_REDUCER,
    examQuestion: EXAM_REDUCER,
    discussion: DISCUSSION_REDUCER,
    cart: CART_REDUCER,
    order: ORDER_REDUCER,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
