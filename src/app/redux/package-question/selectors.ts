import { RootState } from "../store";
import { createSelector } from "@reduxjs/toolkit";

export const PACKAGE_SELECTOR = (state: RootState) => state.products;

export const PACKAGE_SELECTOR_COLLECTION = createSelector(
  PACKAGE_SELECTOR,
  (state) => state
);
