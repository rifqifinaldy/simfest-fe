import { API } from "@/simtest-config/api-collection";
import { REQUEST } from "@/simtest-config/axios";
import { IProductGetParams } from "@/simtest-type/products/product.interface";
import { createAsyncThunk } from "@reduxjs/toolkit";

// REQUEST PACKAGE LIST
export const REQUEST_PACKAGE_LIST = createAsyncThunk(
  "package/list",
  async (params: IProductGetParams, { rejectWithValue }) => {
    try {
      const response = await REQUEST.get(API.V1.PACKAGE_QUESTION.PRODUCT_LIST, {
        params,
      });
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

// REQUEST BOUGHT PACKAGE LIST
export const REQUEST_MY_PACKAGE = createAsyncThunk(
  "package/my-package",
  async (params: IProductGetParams, { rejectWithValue }) => {
    try {
      const response = await REQUEST.get(API.V1.PACKAGE_QUESTION.MY_PACKAGE, {
        params,
      });
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

// REQUEST PACKAGE QUESTION
export const REQUEST_PACKAGE_HOMEPAGE = createAsyncThunk(
  "package/homepage",
  async (_, { rejectWithValue }) => {
    try {
      const response = await REQUEST.get(API.V1.PACKAGE_QUESTION.HOMEPAGE);
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
