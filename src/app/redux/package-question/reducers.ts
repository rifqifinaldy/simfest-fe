import { AxiosError } from "axios";
import { createReducer } from "@reduxjs/toolkit";
import {
  REQUEST_MY_PACKAGE,
  REQUEST_PACKAGE_HOMEPAGE,
  REQUEST_PACKAGE_LIST,
} from "./actions";
import {
  IProductMeta,
  IProductRes,
} from "@/simtest-type/products/product.interface";
import { MyPackageList } from "@/simtest-type/package/my-package.interface";
import { IQuestionPackageRes } from "@/simtest-type/package/package.interface";

export type IQuestionState = {
  packageList: IProductRes[] | [];
  packageListMeta: IProductMeta | null;
  myPackage: MyPackageList[] | [];
  homepagePackage: IQuestionPackageRes | [];
  pending: boolean;
  error: AxiosError | null;
  success: boolean;
};

const initialState: IQuestionState = {
  homepagePackage: [],
  packageListMeta: null,
  packageList: [],
  myPackage: [],
  pending: false,
  error: null,
  success: false,
};

export const PACKAGE_REDUCER = createReducer(initialState, (builder) => {
  builder
    // PACKAGE LIST
    .addCase(REQUEST_PACKAGE_LIST.pending, (state) => {
      state.pending = true;
      state.packageList = [];
    })
    .addCase(REQUEST_PACKAGE_LIST.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.success = true;
      state.packageList = payload.data;
      state.packageListMeta = payload.metadata;
    })
    .addCase(REQUEST_PACKAGE_LIST.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
    })
    // MY PACKAGE
    .addCase(REQUEST_MY_PACKAGE.pending, (state) => {
      state.pending = true;
    })
    .addCase(REQUEST_MY_PACKAGE.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.success = true;
      state.myPackage = payload.data;
    })
    .addCase(REQUEST_MY_PACKAGE.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
    })
    // HOMEPAGE PACKAGE
    .addCase(REQUEST_PACKAGE_HOMEPAGE.pending, (state) => {
      state.pending = true;
    })
    .addCase(REQUEST_PACKAGE_HOMEPAGE.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.success = true;
      state.homepagePackage = payload.data;
    })
    .addCase(REQUEST_PACKAGE_HOMEPAGE.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
    });
});
