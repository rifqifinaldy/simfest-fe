import { RootState } from "../store";
import { createSelector } from "@reduxjs/toolkit";

export const DISCUSSION_SELECTOR = (state: RootState) => state.discussion;

export const DISCUSSION_SELECTOR_COLLECTION = createSelector(
  DISCUSSION_SELECTOR,
  (state) => state
);
