import { IDiscussionResponse } from "@/simtest-type/discussion/discussion.interface";
import { createAsyncThunk } from "@reduxjs/toolkit";

// REQUEST PACKAGE QUESTION
export const STORE_DISCUSSION = createAsyncThunk(
  "question/Discussion",
  (payload: { data: IDiscussionResponse }, { rejectWithValue }) => {
    if (payload.data.code === 200) {
      return payload.data;
    } else {
      return rejectWithValue("Error");
    }
  }
);
