import { AxiosError } from "axios";
import { createReducer } from "@reduxjs/toolkit";
import { STORE_DISCUSSION } from "./actions";
import { IDiscussionQuestions } from "@/simtest-type/discussion/discussion.interface";

type IExamQuestionState = {
  correct: number;
  incorrect: number;
  time_spent: number;
  discussion: IDiscussionQuestions[] | [];
  pending: boolean;
  error: AxiosError | null;
  success: boolean;
};

const initialState: IExamQuestionState = {
  discussion: [],
  correct: 0,
  incorrect: 0,
  time_spent: 0,
  pending: false,
  error: null,
  success: false,
};

export const DISCUSSION_REDUCER = createReducer(initialState, (builder) => {
  builder
    // QUESTIONS
    .addCase(STORE_DISCUSSION.pending, (state) => {
      state.pending = true;
    })
    .addCase(STORE_DISCUSSION.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.success = true;
      state.discussion = payload.data.questions;
    })
    .addCase(STORE_DISCUSSION.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
    });
});
