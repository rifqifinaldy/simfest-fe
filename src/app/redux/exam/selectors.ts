import { RootState } from "../store";
import { createSelector } from "@reduxjs/toolkit";

export const EXAM_SELECTOR = (state: RootState) => state.examQuestion;

export const EXAM_SELECTOR_COLLECTION = createSelector(
  EXAM_SELECTOR,
  (state) => state
);
