import { API } from "@/simtest-config/api-collection";
import { REQUEST } from "@/simtest-config/axios";
import { IExamSubmit } from "@/simtest-type/exam/exam-answer.interface";
import { IExamResponse } from "@/simtest-type/exam/exam-question.interface";
import { createAsyncThunk } from "@reduxjs/toolkit";

// REQUEST PACKAGE QUESTION
export const STORE_TAKE_EXAM = createAsyncThunk(
  "question/storeTakeExam",
  (payload: { data: IExamResponse }, { rejectWithValue }) => {
    if (payload.data.code === 200) {
      return payload.data;
    } else {
      return rejectWithValue("Error");
    }
  }
);

// REQUEST EXAM SUBMIT
export const REQUEST_SUBMIT_EXAM = createAsyncThunk(
  "question/submitExam",
  async (payload: IExamSubmit, { rejectWithValue }) => {
    try {
      const response = await REQUEST.post(API.V1.EXAM.SUBMIT, payload);
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
