import { AxiosError } from "axios";
import { createReducer } from "@reduxjs/toolkit";
import { IExamQuestion } from "@/simtest-type/exam/exam-question.interface";
import { REQUEST_SUBMIT_EXAM, STORE_TAKE_EXAM } from "./actions";

type IExamQuestionState = {
  examQuestions: IExamQuestion[] | [];
  correct: number;
  incorrect: number;
  time_spent: number;
  pending: boolean;
  error: AxiosError | null;
  success: boolean;
};

const initialState: IExamQuestionState = {
  examQuestions: [],
  correct: 0,
  incorrect: 0,
  time_spent: 0,
  pending: false,
  error: null,
  success: false,
};

export const EXAM_REDUCER = createReducer(initialState, (builder) => {
  builder
    // STORE TAKE EXAM
    .addCase(STORE_TAKE_EXAM.pending, (state) => {
      state.pending = true;
    })
    .addCase(STORE_TAKE_EXAM.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.success = true;
      if (payload.data.questions) {
        state.examQuestions = payload.data.questions;
      } else {
        state.examQuestions = [];
      }
    })
    .addCase(STORE_TAKE_EXAM.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
    })
    // SUBMIT EXAM
    .addCase(REQUEST_SUBMIT_EXAM.pending, (state) => {
      state.pending = true;
    })
    .addCase(REQUEST_SUBMIT_EXAM.fulfilled, (state) => {
      state.pending = false;
      state.success = true;
    })
    .addCase(REQUEST_SUBMIT_EXAM.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
    });
});
