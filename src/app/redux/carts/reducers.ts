import { AxiosError } from "axios";
import { createReducer } from "@reduxjs/toolkit";
import {
  REQUEST_ADD_CART,
  REQUEST_CART_LIST,
  REQUEST_DELETE_CART,
  REQUEST_RESET_CART,
  REQUEST_RESET_VOUCHER,
} from "./actions";
import { ICartResponse } from "@/simtest-type/carts/carts.interface";
import { IVoucherDiscount } from "@/simtest-type/carts/voucher.interface";

type ICartQuestionState = {
  voucher: IVoucherDiscount;
  cartList: ICartResponse[] | [];
  pending: boolean;
  error: AxiosError | null;
  success: boolean;
  refetch: boolean;
  totalChartItem: number;
};

const initialVoucher: IVoucherDiscount = {
  amount: 0,
  total_amount: 0,
  total_discount: 0,
  voucher_discount: 0,
  voucher_message: "",
  voucher_code: "",
  voucher_status: false,
};

const initialState: ICartQuestionState = {
  voucher: initialVoucher,
  cartList: [],
  pending: false,
  error: null,
  success: false,
  refetch: false,
  totalChartItem: 0,
};

export const CART_REDUCER = createReducer(initialState, (builder) => {
  builder
    // GET ALL CARTS
    .addCase(REQUEST_CART_LIST.pending, (state) => {
      state.pending = true;
    })
    .addCase(REQUEST_CART_LIST.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.success = true;
      state.cartList = payload.data;
      state.voucher = payload.metadata;
      state.totalChartItem = payload?.data?.length || 0;
    })
    .addCase(REQUEST_CART_LIST.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
    })
    // Add Item to Cart
    .addCase(REQUEST_ADD_CART.pending, (state) => {
      state.pending = true;
      state.refetch = false;
    })
    .addCase(REQUEST_ADD_CART.fulfilled, (state) => {
      state.pending = false;
      state.success = true;
      state.refetch = true;
    })
    .addCase(REQUEST_ADD_CART.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
      state.refetch = true;
    })
    // Delete Item from Cart
    .addCase(REQUEST_DELETE_CART.pending, (state) => {
      state.pending = true;
      state.refetch = false;
    })
    .addCase(REQUEST_DELETE_CART.fulfilled, (state) => {
      state.pending = false;
      state.success = true;
      state.refetch = true;
    })
    .addCase(REQUEST_DELETE_CART.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
      state.refetch = true;
    })
    // RESET VOUCHER
    .addCase(REQUEST_RESET_VOUCHER, (state) => {
      state.voucher = initialState.voucher;
      state.error = initialState.error;
    })
    // RESET CART
    .addCase(REQUEST_RESET_CART, (state) => {
      state.cartList = initialState.cartList;
      state.totalChartItem = initialState.totalChartItem;
      state.error = initialState.error;
      state.success = initialState.success;
      state.refetch = initialState.refetch;
    });
});
