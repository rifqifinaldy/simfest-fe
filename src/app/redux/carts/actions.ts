import { API } from "@/simtest-config/api-collection";
import { REQUEST } from "@/simtest-config/axios";
import { createAction, createAsyncThunk } from "@reduxjs/toolkit";

// REQUEST CART LIST
export const REQUEST_CART_LIST = createAsyncThunk(
  "cart/cartList",
  async (voucher: string, { rejectWithValue }) => {
    try {
      const response = await REQUEST.get(API.V1.CARTS.CARTS, {
        params: { voucher: voucher },
      });
      if (response.data.metadata.voucher_message) {
        return rejectWithValue(response.data.metadata.voucher_message);
      }
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

// REQUEST CART LIST
export const REQUEST_ADD_CART = createAsyncThunk(
  "cart/addCart",
  async (package_question_id: string, { rejectWithValue }) => {
    try {
      const response = await REQUEST.post(API.V1.CARTS.CARTS, {
        package_question_id,
      });
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

// REQUEST CART LIST
export const REQUEST_DELETE_CART = createAsyncThunk(
  "cart/delete",
  async (id: number, { rejectWithValue }) => {
    try {
      const response = await REQUEST.post(API.V1.CARTS.CART_DELETE, {
        id,
      });
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

// RESET VOUCHER
export const REQUEST_RESET_VOUCHER = createAction("voucher/reset");
// RESET CART
export const REQUEST_RESET_CART = createAction("cart/reset");
