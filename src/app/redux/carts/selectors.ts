import { RootState } from "../store";
import { createSelector } from "@reduxjs/toolkit";

export const CART_SELECTOR = (state: RootState) => state.cart;

export const CART_SELECTOR_COLLECTION = createSelector(
  CART_SELECTOR,
  (state) => state
);
