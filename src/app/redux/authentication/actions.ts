import { API } from "@/simtest-config/api-collection";
import { REQUEST } from "@/simtest-config/axios";
import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import { ILoginInput, IRegisterInput } from "type/auth/auth.interface";

// REQUEST LOGIN
export const REQUEST_SIGN_IN = createAsyncThunk(
  "auth/login",
  async (data: ILoginInput, { rejectWithValue }) => {
    try {
      const response = await REQUEST.post(API.V1.AUTH.LOGIN, data);
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

// REQUEST REGUSTER
export const REQUEST_REGISTER = createAsyncThunk(
  "auth/register",
  async (data: IRegisterInput, { rejectWithValue }) => {
    try {
      const response = await REQUEST.post(API.V1.AUTH.REGISTER, data);
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const REQUEST_SIGN_IN_GOOGLE = createAsyncThunk(
  "auth/loginGoogle",
  async (token: string, { rejectWithValue }) => {
    try {
      const response = await REQUEST.post(API.V1.AUTH.GOOGLE_AUTH, {
        token: token,
      });
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const REQUEST_SIGN_OUT = createAction("users/logout");
