import { createReducer } from "@reduxjs/toolkit";
import {
  REQUEST_REGISTER,
  REQUEST_SIGN_IN,
  REQUEST_SIGN_IN_GOOGLE,
} from "./actions";
import { AxiosError } from "axios";
import { IAuthResponse } from "type/auth/auth.interface";

export type IAuthState = {
  data: IAuthResponse | null;
  pending: boolean;
  error: AxiosError | null;
  success: boolean;
};

const initialState: IAuthState = {
  data: null,
  pending: false,
  error: null,
  success: false,
};

export const AUTH_REDUCER = createReducer(initialState, (builder) => {
  builder
    // LOG IN REDUCER
    .addCase(REQUEST_SIGN_IN.pending, (state) => {
      state.pending = true;
    })
    .addCase(REQUEST_SIGN_IN.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.success = true;
      state.data = payload;
    })
    .addCase(REQUEST_SIGN_IN.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
    })
    // REGISTER REDUCER
    .addCase(REQUEST_REGISTER.pending, (state) => {
      state.pending = true;
    })
    .addCase(REQUEST_REGISTER.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.success = true;
      state.data = payload;
    })
    .addCase(REQUEST_REGISTER.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
    })
    // GOOGLE SIGN IN
    .addCase(REQUEST_SIGN_IN_GOOGLE.pending, (state) => {
      state.pending = true;
    })
    .addCase(REQUEST_SIGN_IN_GOOGLE.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.success = true;
      state.data = payload;
    })
    .addCase(REQUEST_SIGN_IN_GOOGLE.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
    });
});
