import { AxiosError } from "axios";
import { createReducer } from "@reduxjs/toolkit";
import { REQUEST_ORDER, REQUEST_ORDER_HISTORY } from "./actions";
import {
  IOrderData,
  IOrderResponse,
} from "@/simtest-type/order/order.interface";

type IExamQuestionState = {
  order: IOrderResponse | null;
  orderList: IOrderData[] | [];
  pending: boolean;
  error: AxiosError | null;
  success: boolean;
  refetch: boolean;
};

const initialState: IExamQuestionState = {
  order: null,
  orderList: [],
  pending: false,
  error: null,
  success: false,
  refetch: false,
};

export const ORDER_REDUCER = createReducer(initialState, (builder) => {
  builder
    // CREATE ORDER
    .addCase(REQUEST_ORDER.pending, (state) => {
      state.pending = true;
    })
    .addCase(REQUEST_ORDER.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.success = true;
      state.order = payload.data;
    })
    .addCase(REQUEST_ORDER.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
    })
    // LIST HISTORY ORDER
    .addCase(REQUEST_ORDER_HISTORY.pending, (state) => {
      state.pending = true;
    })
    .addCase(REQUEST_ORDER_HISTORY.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.success = true;
      state.orderList = payload.data;
    })
    .addCase(REQUEST_ORDER_HISTORY.rejected, (state, { payload }) => {
      state.pending = false;
      state.error = payload as AxiosError;
    });
});
