import { API } from "@/simtest-config/api-collection";
import { REQUEST } from "@/simtest-config/axios";
import { createAsyncThunk } from "@reduxjs/toolkit";

// REQUEST Create Order
export const REQUEST_ORDER = createAsyncThunk(
  "order/create",
  async (voucher: string, { rejectWithValue }) => {
    try {
      const response = await REQUEST.post(API.V1.ORDER.ORDER, { voucher });
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

//  REQUEST ORDER HISTORY LIST
export const REQUEST_ORDER_HISTORY = createAsyncThunk(
  "order/history",
  async (_, { rejectWithValue }) => {
    try {
      const response = await REQUEST.get(API.V1.ORDER.HISTORY);
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
