import { RootState } from "../store";
import { createSelector } from "@reduxjs/toolkit";

export const ORDER_SELECTOR = (state: RootState) => state.order;

export const ORDER_SELECTOR_COLLECTION = createSelector(
  ORDER_SELECTOR,
  (state) => state
);
