import useDiscussion from "@/simtest-hook/useDiscussion";
import {
  IDiscussionQuestions,
  IDiscussionResponse,
} from "@/simtest-type/discussion/discussion.interface";
import { createContext, useEffect } from "react";

export interface IDiscussionContext {
  discussion: IDiscussionQuestions[] | [];
  activeQuestion: IDiscussionQuestions;
  activeIndex: number;
  nextAnswer: () => void;
  prevAnswer: () => void;
  selectQuestion: (id: number) => void;
}

interface DiscussionProviderProps {
  children: React.ReactNode;
  data: IDiscussionResponse;
  error: boolean;
}

export const DiscussionContext = createContext<IDiscussionContext | undefined>(
  undefined
);

const DiscussionProvider: React.FC<DiscussionProviderProps> = ({
  children,
  data,
  error,
}) => {
  const {
    prevAnswer,
    nextAnswer,
    storeDiscussion,
    discussion,
    activeQuestion,
    activeIndex,
    selectQuestion,
  } = useDiscussion();

  useEffect(() => {
    if (!error) {
      storeDiscussion(data);
    } else {
      alert("Opps something went wrong");
    }
  }, [data, error, storeDiscussion]);

  return (
    <DiscussionContext.Provider
      value={{
        prevAnswer,
        nextAnswer,
        activeQuestion,
        activeIndex,
        discussion,
        selectQuestion,
      }}
    >
      {children}
    </DiscussionContext.Provider>
  );
};

export default DiscussionProvider;
