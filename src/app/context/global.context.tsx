import SimfestDrawer, {
  SimfestDrawerProps,
} from "@/simtest-components/drawer/simfest-drawer";
import useDrawer from "@/simtest-components/drawer/simfest-drawer/hook";
import SimfestModal, {
  SimfestModalProps,
} from "@/simtest-components/popup/simfest-modal";
import useModal from "@/simtest-components/popup/simfest-modal/hook";
import useProfile from "@/simtest-hook/useProfile";
import { IAuthResponse } from "@/simtest-type/auth/auth.interface";
import { createContext } from "react";

export interface IGlobalContext {
  modal?: SimfestModalProps;
  drawer?: SimfestDrawerProps;
  user: {
    profile: IAuthResponse | null;
    updateProfile: (data: IAuthResponse) => void;
  };
}

interface GlobalProviderProps {
  children: React.ReactNode;
}

export const GlobalContext = createContext<IGlobalContext | undefined>(
  undefined
);

const GlobalProvider: React.FC<GlobalProviderProps> = ({ children }) => {
  const contextValue = {
    modal: useModal(),
    user: useProfile(),
    drawer: useDrawer(),
  };

  return (
    <GlobalContext.Provider value={contextValue}>
      {children}
      <SimfestModal {...contextValue.modal} />
      <SimfestDrawer {...contextValue.drawer} />
    </GlobalContext.Provider>
  );
};

export default GlobalProvider;
