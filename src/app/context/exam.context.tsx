import useExam, { IInitialAnswer } from "@/simtest-hook/useExam";
import {
  IExamQuestion,
  IExamResponse,
} from "@/simtest-type/exam/exam-question.interface";
import { Dispatch, SetStateAction, createContext, useEffect } from "react";

export interface IExamContext {
  questions: IExamQuestion[] | [];
  activeQuestion: IExamQuestion;
  setActiveQuestion: Dispatch<SetStateAction<IExamQuestion>>;
  answers: IInitialAnswer[];
  activeIndex: number;
  selectQuestion: (id: number) => void;
  nextAnswer: () => void;
  prevAnswer: () => void;
  handleAnswer: (id: number, status: string, value: string) => void;
  step: number;
  timer: { hours: number; minutes: number; seconds: number };
  submitExam: (payload: IInitialAnswer[]) => void;
}

interface ExamProviderProps {
  children: React.ReactNode;
  data: IExamResponse;
  error: boolean;
}

export const ExamContext = createContext<IExamContext | undefined>(undefined);

const ExamProvider: React.FC<ExamProviderProps> = ({
  children,
  error,
  data,
}) => {
  const {
    activeIndex,
    storeTakeExam,
    questions,
    answers,
    activeQuestion,
    setActiveQuestion,
    selectQuestion,
    nextAnswer,
    prevAnswer,
    handleAnswer,
    step,
    timer,
    submitExam,
  } = useExam();

  useEffect(() => {
    if (!error) {
      storeTakeExam(data);
    } else {
      alert("Opps Terjadi Kesalahan");
    }
  }, [data, error, storeTakeExam]);

  return (
    <ExamContext.Provider
      value={{
        activeIndex,
        questions,
        activeQuestion,
        setActiveQuestion,
        answers,
        selectQuestion,
        nextAnswer,
        prevAnswer,
        handleAnswer,
        step,
        timer,
        submitExam,
      }}
    >
      {children}
    </ExamContext.Provider>
  );
};

export default ExamProvider;
