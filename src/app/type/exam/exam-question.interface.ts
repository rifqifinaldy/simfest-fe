export enum EExamQuestionOption {
  STRING_OPTION = "string",
  IMAGE_OPTION = "image",
}

export interface IExamQuestionOption {
  type: `${EExamQuestionOption}`;
  value: string;
}

export interface IExamQuestion {
  id: number;
  question: string;
  answer: string;
  discussion: string;
  discussion_content: string;
  content_one: string;
  content_two: string;
  content_text: string;
  option: IExamQuestionOption[];
}

export interface IExamQuestions {
  questions: IExamQuestion[];
  correct: number;
  incorrect: number;
  time_spent: number;
}

export interface IExamResponse {
  code: number;
  data: IExamQuestions;
  msg: {
    id: string;
    en: string;
  };
}
