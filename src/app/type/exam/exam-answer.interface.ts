export enum EAnswerType {
  NULL = "",
  NOT_ANSWERED = "NOT_ANSWERED",
  ANSWERED = "ANSWERED",
  HESISTANT = "HESISTANT",
  ON_GOING = "ON_GOING",
}

export interface IExamSubmitAnswer {
  question_id: number;
  answer: string;
}
export interface IExamSubmit {
  user_section_id: string;
  time_spent: number;
  exam_question_answer: IExamSubmitAnswer[];
}
