export enum NAVIGATION {
  HOMEPAGE = "/",
  AUTH_LOGIN = "/auth/login",
  AUTH_REGISTER = "/auth/register",
  DASHBOARD_HOME = "/dashboard",
  DASHBOARD_MY_EXAM = "/dashboard/my-exam",
  DASHBOARD_PRODUCT = "/dashboard/products",
  DASHBOARD_TRANSACTION = "/dashboard/transaction",
  DASHBOARD_CART = "/dashboard/cart",
  EXAM = "/exam/",
  DISCUSSION = "/discussion",
}
