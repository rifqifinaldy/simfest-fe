import { IQuestionPackageRes } from "../package/package.interface";

export interface IOrderResponse {
  order_id: string;
  payment_url: string;
  token: string;
}

export interface IOrderDetail {
  Id: number;
  package: IQuestionPackageRes;
}

export interface IOrderData {
  id: string;
  discount: number;
  total_amount: number;
  payment_token: string;
  payment_url: string;
  payment_method: string;
  payment_channel: string;
  status: string;
  details: IOrderDetail[];
  created_at: string;
  paid_at?: string;
  expired_at?: string;
}
