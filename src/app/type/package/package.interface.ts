export enum ETestType {
  BUMN = "BUMN",
  LPDP = "LPDP",
  CPNS = "CPNS",
}

export type TTestType = `${ETestType}`;

export interface ISectionPackage {
  id: number;
  name: string;
  package_question_id: string;
  sort: number;
  time: number;
  total_question: number;
}

export interface IQuestionPackageRes {
  id: string;
  name: string;
  time: number;
  total_question: number;
  price: number;
  price_before_discount: number;
  discount_percent: number;
  user_package_id: number;
  package: IPackage;
  sections: ISectionPackage[];
}

export interface IPackage {
  id: number;
  name: `${ETestType}`;
}
