import { IProductSections } from "../products/product.interface";
import { IQuestionPackageRes } from "./package.interface";

export enum EPackageName {
  BUMN = "BUMN",
  LPDP = "LPDP",
  CPNS = "CPNS",
}

interface IPackage {
  id: number;
  name: `${EPackageName}`;
  image: string;
  color: string;
  created_at: string;
}

interface IMyPackage {
  id: number;
  package_question: IQuestionPackageRes;
  sections: IProductSections[] | [];
  order_id: string;
  created_at: string;
}

export interface MyPackageList {
  package: IPackage;
  my_package: IMyPackage[];
}
