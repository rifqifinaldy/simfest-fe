export interface ILoginInput {
  email: string;
  password: string;
}

export interface IRegisterInput {
  name: string;
  email: string;
  password: string;
}

export interface IAuthResponse {
  name: string;
  email: string;
  token: string;
}
