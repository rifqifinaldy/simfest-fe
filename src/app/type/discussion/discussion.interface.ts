export interface IDiscussionQuestionOption {
  is_answer: boolean;
  is_chosen: boolean;
  type: string;
  value: string;
}

export interface IDiscussionQuestions {
  id: number;
  content_one: string;
  content_two: string;
  content_text: string;
  discussion: string;
  discussion_content: string;
  option: IDiscussionQuestionOption[];
  question: string;
  answer: string;
}

export interface IDiscussionData {
  correct: number;
  incorrect: number;
  questions: IDiscussionQuestions[];
  time_spent: number;
}

export interface IDiscussionResponse {
  code: number;
  data: IDiscussionData;
  msg: {
    id: string;
    en: string;
  };
}
