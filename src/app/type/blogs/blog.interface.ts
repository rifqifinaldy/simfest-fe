import { Entry, EntryFields } from "contentful";
import { IThumbnail } from "../contentful/contentful.interface";

interface BlogType {
  fields: {
    author: EntryFields.Text;
    title: EntryFields.Text;
    thumbnail: IThumbnail;
    postedAt: EntryFields.Text;
    content: EntryFields.RichText;
    highlightText: EntryFields.Text;
    slug: string;
    tags: EntryFields.Array<string>;
    bannerImage: IThumbnail;
  };
  contentTypeId: string;
}

export type IBlog = Entry<BlogType>;
