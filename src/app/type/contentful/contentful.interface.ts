import { Entry, EntryCollection, EntrySkeletonType } from "contentful";

export type ContentfulReturnType = EntryCollection<
  EntrySkeletonType,
  undefined,
  string
>;

export interface IThumbnail {
  fields: {
    title: string;
    file: {
      url: string;
    };
  };
}

export type ContentfulItems = Entry<EntrySkeletonType, undefined, string>[];
