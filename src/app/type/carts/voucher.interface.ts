export interface IVoucherDiscount {
  amount: number;
  total_amount: number;
  total_discount: number;
  voucher_discount: number;
  voucher_message: string;
  voucher_status: boolean;
  voucher_code: string;
}
