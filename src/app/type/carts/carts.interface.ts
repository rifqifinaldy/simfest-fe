import { IQuestionPackageRes } from "../package/package.interface";

export interface ICartResponse {
  id: number;
  type: string;
  package_question: IQuestionPackageRes;
}
