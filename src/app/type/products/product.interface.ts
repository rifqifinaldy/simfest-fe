import { ISectionPackage } from "../package/package.interface";

export enum EProductType {
  BUMN = "BUMN",
  LPDP = "LPDP",
  CPNS = "CPNS",
}

export type TProductType = `${EProductType}`;

export interface IProductRes {
  id: string;
  name: string;
  time: number;
  total_question: number;
  price: number;
  price_before_discount: number;
  discount_percent: number;
  package: IProduct;
  user_package_id: number;
  sections: ISectionPackage[];
}

export interface IProductMeta {
  count: number;
  length: number;
  page: number;
  total_page: number;
}

export interface IProduct {
  id: number;
  name: `${EProductType}`;
}

export interface IProductSections {
  correct_answer: number;
  id: string;
  exam_id?: string;
  name: string;
  sort: 0;
  status: string;
  time: 30;
  total_question: 28;
}

export interface IMyProduct {
  id: number;
  created_at: string;
  order_id: string;
  package_question: IProductRes;
  sections: IProductSections[] | [];
}

export interface IProductGetParams {
  page?: number;
  length?: number;
  package_id?: number;
}
