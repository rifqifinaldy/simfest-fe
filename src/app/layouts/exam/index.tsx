import { Box, ChakraBaseProvider, Flex } from "@chakra-ui/react";
import React from "react";
import theme from "../../../styles/theme";
import GlobalProvider from "@/simtest-context/global.context";
import { Provider } from "react-redux";
import { store } from "@/simtest-redux/store";

interface ExamLayoutProps {
  children: React.ReactNode;
}

const ExamLayout = ({ children }: ExamLayoutProps) => {
  return (
    <Provider store={store}>
      <ChakraBaseProvider resetCSS theme={theme}>
        <GlobalProvider>
          <Flex>
            <Box background="#F4F7FE" w="full" p={{ base: "10px", lg: "40px" }}>
              {children}
            </Box>
          </Flex>
        </GlobalProvider>
      </ChakraBaseProvider>
    </Provider>
  );
};

export default ExamLayout;
