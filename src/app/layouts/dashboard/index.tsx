import { Box, ChakraBaseProvider, Flex } from "@chakra-ui/react";
import React from "react";
import theme from "../../../styles/theme";
import GlobalProvider from "@/simtest-context/global.context";
import { Provider } from "react-redux";
import { store } from "@/simtest-redux/store";
import HelpCenter from "@/simtest-components/help-center";
import DekstopNavigation from "@/simtest-components/sidebar/dekstop";
import MobileNavigation from "@/simtest-components/sidebar/mobile";
import MobileTopbar from "@/simtest-components/sidebar/mobile-topbar";

interface DashboardLayoutProps {
  children: React.ReactNode;
}

const DashboardLayout = ({ children }: DashboardLayoutProps) => {
  return (
    <Provider store={store}>
      <ChakraBaseProvider resetCSS theme={theme}>
        <GlobalProvider>
          <MobileTopbar />
          <Flex h="100vh" overflow="hidden" position="relative">
            <DekstopNavigation />
            <Box
              background="#F4F7FE"
              w="full"
              px={{ base: "10px", md: "40px" }}
              py="80px"
              overflow="auto"
            >
              {children}
            </Box>
            <MobileNavigation />
            <Box
              display={{ base: "none", lg: "block" }}
              position="absolute"
              bottom={{ base: "0", lg: "20px" }}
              right="20px"
            >
              <HelpCenter />
            </Box>
          </Flex>
        </GlobalProvider>
      </ChakraBaseProvider>
    </Provider>
  );
};

export default DashboardLayout;
