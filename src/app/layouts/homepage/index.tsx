import Navbar from "@/simtest-components/navbar";
import { Box, ChakraBaseProvider } from "@chakra-ui/react";
import React from "react";
import theme from "../../../styles/theme";
import Footer from "@/simtest-components/footer";
import GlobalProvider from "@/simtest-context/global.context";
import { Provider } from "react-redux";
import { store } from "@/simtest-redux/store";

interface HomepageLayoutProps {
  children: React.ReactNode;
}

const HomepageLayout = ({ children }: HomepageLayoutProps) => {
  return (
    <Provider store={store}>
      <ChakraBaseProvider resetCSS theme={theme}>
        <GlobalProvider>
          <Navbar />
          <Box
            style={{ scrollBehavior: "smooth" }}
            as="main"
            px={{ base: "20px", lg: "80px" }}
          >
            {children}
          </Box>
          <Footer />
        </GlobalProvider>
      </ChakraBaseProvider>
    </Provider>
  );
};

export default HomepageLayout;
