import { Box, ChakraBaseProvider, Flex, Image } from "@chakra-ui/react";
import React from "react";
import theme from "../../../styles/theme";

interface AuthLayoutProps {
  children: React.ReactNode;
}

const AuthLayout = ({ children }: AuthLayoutProps) => {
  return (
    <ChakraBaseProvider resetCSS theme={theme}>
      <Box as="main">
        <Flex
          h="100vh"
          px={{ base: "0", lg: "5em" }}
          py="10em"
          justifyContent="space-between"
          alignItems="center"
        >
          <Box
            display={{ base: "none", lg: "block" }}
            borderRight={{ base: "none", lg: "1px solid #0A033C" }}
            w="50%"
          >
            <Image
              src="/assets/auth/auth-illustration.svg"
              alt="simulasitest"
              w="75%"
            />
          </Box>
          <Box zIndex={20} w={{ base: "full", lg: "50%" }} px="5em">
            {children}
          </Box>
        </Flex>
      </Box>
    </ChakraBaseProvider>
  );
};

export default AuthLayout;
