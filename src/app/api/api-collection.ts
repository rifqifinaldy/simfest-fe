export const API = {
  V1: {
    AUTH: {
      LOGIN: "/v1/user/login",
      REGISTER: "/v1/user/register",
      GOOGLE_AUTH: "/v1/user/login/google",
    },
    CARTS: {
      CARTS: "/v1/carts",
      CART_DELETE: "/v1/carts/delete",
    },
    ORDER: {
      ORDER: "/v1/orders",
      HISTORY: "/v1/orders/history",
    },
    PACKAGE_QUESTION: {
      HOMEPAGE: "/v1/package-question/homepage",
      PRODUCT_LIST: "/v1/package-question",
      MY_PACKAGE: "/v1/package-question/my-packages",
    },
    EXAM: {
      TAKE: "/v1/package-question/exam",
      SUBMIT: "/v1/package-question/exam",
      DISCUSSION: (id: string) => `/v1/package-question/exam/${id}/discussion`,
    },
  },
  OTHERS: {
    GOOGLE_CALLBACK: "/auth/google/callback",
  },
};
