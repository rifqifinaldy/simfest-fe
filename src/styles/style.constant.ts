export enum RADIUS {
  XXS = "4px",
  XS = "8px",
  SM = "12px",
  LG = "16px",
  XL = "20px",
}

export enum COLORS {
  GRAY = "var(--chakra-colors-neutral-300)",
}

export enum BORDER {
  DEFAULT = "1px solid var(--chakra-colors-neutral-300)",
}
