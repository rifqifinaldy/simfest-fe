import { extendTheme } from "@chakra-ui/react";
import { TEXT_VARIANT } from "./default-styles/text/text-variant";
import { formStyles } from "./default-styles/form/form-style";

const colors = {
  neutral: {
    0: "#FFFFFF",
    100: "#F8F9FC",
    200: "#F1F3F9",
    300: "#E1E6EF",
    700: "#3F444D",
    800: "#23272F",
    900: "#1B1F27",
    1000: "#0A0D14",
  },
  primary: {
    100: "#F0F5FF",
    700: "#2F6FED",
    800: "#1D5BD6",
    900: "#1E4EAE",
  },
  secondary: {
    100: "#FFF6DD",
    700: "#FFC007",
    800: "#D59B2A",
    900: "#A37B05",
  },
  success: {
    100: "#EDFDF8",
    700: "#08875D",
    800: "#04724D",
    900: "#066042",
  },
  warning: {
    100: "#FFF8EB",
    700: "#B25E09",
    800: "#96530F",
    900: "#80460D",
  },
  danger: {
    100: "#FEF1F2",
    700: "#E02D3C",
    800: "#BA2532",
    900: "#981B25",
  },
  text: {
    "primary-black": "#1D2433",
    "secondary-black": "#353d4f",
    "disabled-black": "#54565a",
    "primary-white": "#FFFFFF",
    "secondary-white": "#FFFFFF",
    "disabled-white": "#FFFFFF",
  },
};

const breakpoints = {
  sm: "480px",
  md: "821px",
  lg: "1024px",
  xl: "1280px",
  "2xl": "1536px",
};

const theme = extendTheme({
  colors,
  breakpoints,
  styles: {
    global: () => ({
      body: {
        backgroundColor: colors.neutral["0"],
        fontSize: {
          base: "14px",
          md: "16px",
        },
        color: colors.text["primary-black"],
      },
      a: {
        color: "primary.700",
      },
    }),
  },
  components: {
    Button: {
      baseStyle: {
        letterSpacing: "0.03em",
        px: "100px",
        fontWeight: 500,
        fontSize: "24px",
        transition:
          "background-color 400ms ease, border-color 400ms ease, transform 400ms ease",
      },
    },
    Text: {
      variants: {
        ...TEXT_VARIANT,
      },
    },
    Form: formStyles,
  },
});

export default theme;
