export enum EFontWeights {
  REGULAR = "400",
  MEDIUM = "500",
  SEMIBOLD = "600",
  BOLD = "700",
}

export const BodyXLarge = {
  as: "p",
  color: "#1D2433",
  fontSize: "18px",
  lineHeight: "25.2px",
  letterSpacing: "0.2px",
};

export const BodyLarge = {
  as: "p",
  color: "#1D2433",
  fontSize: "16px",
  lineHeight: "22.4px",
  letterSpacing: "0.2px",
};

export const BodyMedium = {
  as: "p",
  color: "#1D2433",
  fontSize: "14px",
  lineHeight: "19.6px",
  letterSpacing: "0.2px",
};

export const BodySmall = {
  as: "p",
  color: "#1D2433",
  fontSize: "12px",
  lineHeight: "12.52px",
  letterSpacing: "0.2px",
};

export const BodyXSmall = {
  as: "p",
  color: "#1D2433",
  fontSize: "10px",
  lineHeight: "12.1px",
};

export const TEXT_VARIANT = {
  // Heading
  "heading-h1": {
    as: "h1",
    fontWeight: EFontWeights.BOLD,
    fontSize: "48px",
    lineHeight: "52.8px",
  },
  "heading-h2": {
    as: "h2",
    fontWeight: EFontWeights.BOLD,
    fontSize: "40px",
    lineHeight: "44px",
  },
  "heading-h3": {
    as: "h3",
    fontWeight: EFontWeights.BOLD,
    fontSize: "32px",
    lineHeight: "35.2px",
  },
  "heading-h4": {
    as: "h4",
    fontWeight: EFontWeights.BOLD,
    fontSize: "24px",
    lineHeight: "28.8px",
  },
  "heading-h5": {
    as: "h5",
    fontWeight: EFontWeights.BOLD,
    fontSize: "20px",
    lineHeight: "24px",
  },
  "heading-h6": {
    as: "h6",
    fontWeight: EFontWeights.BOLD,
    fontSize: "18px",
    lineHeight: "21.6px",
  },
  heading7: {
    as: "h6",
    fontWeight: EFontWeights.MEDIUM,
    fontSize: "20px",
    lineHeight: "30px",
  },
  heading8: {
    fontWeight: EFontWeights.REGULAR,
    fontSize: "18px",
    lineHeight: "27px",
  },
  // Body XLarge
  "body-xlarge-regular": {
    ...BodyXLarge,
    fontWeight: EFontWeights.REGULAR,
  },
  "body-xlarge-medium": {
    ...BodyXLarge,
    fontWeight: EFontWeights.MEDIUM,
  },
  "body-xlarge-semibold": {
    ...BodyXLarge,
    fontWeight: EFontWeights.SEMIBOLD,
  },
  "body-xlarge-bold": {
    ...BodyXLarge,
    fontWeight: EFontWeights.BOLD,
  },
  // Body Large
  "body-large-regular": {
    ...BodyLarge,
    fontWeight: EFontWeights.REGULAR,
  },
  "body-large-medium": {
    ...BodyLarge,
    fontWeight: EFontWeights.MEDIUM,
  },
  "body-large-semibold": {
    ...BodyLarge,
    fontWeight: EFontWeights.SEMIBOLD,
  },
  "body-large-bold": {
    ...BodyLarge,
    fontWeight: EFontWeights.BOLD,
  },
  // Body Medium
  "body-medium-regular": {
    ...BodyMedium,
    fontWeight: EFontWeights.REGULAR,
  },
  "body-medium-medium": {
    ...BodyMedium,
    fontWeight: EFontWeights.MEDIUM,
  },
  "body-medium-semibold": {
    ...BodyMedium,
    fontWeight: EFontWeights.SEMIBOLD,
  },
  "body-medium-bold": {
    ...BodyMedium,
    fontWeight: EFontWeights.BOLD,
  },
  // Body Small
  "body-small-regular": {
    ...BodySmall,
    fontWeight: EFontWeights.REGULAR,
  },
  "body-small-medium": {
    ...BodySmall,
    fontWeight: EFontWeights.MEDIUM,
  },
  "body-small-semibold": {
    ...BodySmall,
    fontWeight: EFontWeights.SEMIBOLD,
  },
  "body-small-bold": {
    ...BodySmall,
    fontWeight: EFontWeights.BOLD,
  },
  // Body XSmall
  "body-xsmall-regular": {
    ...BodyXSmall,
    fontWeight: EFontWeights.REGULAR,
  },
  "body-xsmall-medium": {
    ...BodyXSmall,
    fontWeight: EFontWeights.MEDIUM,
  },
  "body-xsmall-semibold": {
    ...BodyXSmall,
    fontWeight: EFontWeights.SEMIBOLD,
  },
  "body-xsmall-bold": {
    ...BodyXSmall,
    fontWeight: EFontWeights.BOLD,
  },
};
