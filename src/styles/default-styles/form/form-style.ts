import { ComponentStyleConfig } from "@chakra-ui/react";
import { BodyLarge } from "../text/text-variant";

export const formStyles: ComponentStyleConfig = {
  parts: ["container", "requiredIndicator", "helperText"],
  baseStyle: {
    container: {
      my: "1em",
      label: {
        ...BodyLarge,
        fontWeight: 400,
        mb: "0.25em",
      },
    },
  },
};
