import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";
export function middleware(request: NextRequest) {
  const userToken = request.cookies.get("st");
  if (!userToken) {
    if (request.nextUrl.pathname.startsWith("/auth")) {
      return NextResponse.next();
    }
    if (
      ["/dashboard"].some((path) => request.nextUrl.pathname.startsWith(path))
    ) {
      return NextResponse.redirect(new URL("/auth/login", request.url));
    }
  } else {
    if (request.nextUrl.pathname.startsWith("/auth")) {
      return NextResponse.redirect(new URL("/", request.url));
    }
  }
}
export const config = {
  matcher: ["/auth/:path*", "/dashboard/:path*"],
};
