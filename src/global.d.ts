export {};

declare global {
  interface Window {
    gtag: (...args: unknown[]) => void; // Type for gtag function
  }
}
